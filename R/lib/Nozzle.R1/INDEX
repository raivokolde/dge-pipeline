DEFAULT.DOI.RESOLVER    Default DOI resolver URL.
DEFAULT.REPORT.FILENAME
                        Default filename for reports.
DEFAULT.SIGNIFICANT.ENTITY
                        Name of entities that are labeled as
                        signficiant.
HTML.FRAGMENT           Output type.
HTML.REPORT             Output type.
IMAGE.TYPE.PDF          Image type.
IMAGE.TYPE.RASTER       Image type.
IMAGE.TYPE.SVG          Image type.
LOGO.BOTTOM.CENTER      Logo position.
LOGO.BOTTOM.LEFT        Logo position.
LOGO.BOTTOM.RIGHT       Logo position.
LOGO.TOP.CENTER         Logo position.
LOGO.TOP.LEFT           Logo position.
LOGO.TOP.RIGHT          Logo position.
Nozzle.R1-package       Nozzle: a Report Generation Toolkit for Data
                        Analysis Pipelines
PROTECTION.GROUP        Group visibility.
PROTECTION.PRIVATE      Private visibility.
PROTECTION.PUBLIC       Public visibility.
PROTECTION.TCGA         Group visibility.
RDATA.REPORT            Output type.
SECTION.CLASS.META      Section class.
SECTION.CLASS.RESULTS   Section class.
TABLE.SIGNIFICANT.DIGITS
                        Default number of significant digits to be used
                        to trim numeric columns in tables.
addRelatedReport        Add the URL, title, and signficance status of a
                        related report, e.g. one summarizing the same
                        type of analysis but on a different input set.
addTo                   Add child elements to a parent element.
addToInput              Add elements to the "Input" subsection in the
                        "Methods & Data" section of a standard report.
addToIntroduction       Add elements to the "Introduction" subsection
                        in the "Overview" section of a standard report.
addToMeta               Add elements to the "Meta" section of a
                        standard report.
addToMethods            Add elements to the "Methds & Data" section of
                        a standard report.
addToOverview           Add elements to the "Overview" section of a
                        standard report.
addToReferences         Add elements to the "References" subsection in
                        the "Methods & Data" section of a standard
                        report.
addToResults            Add elements to the "Results" section of a
                        standard report.
addToSummary            Add elements to the "Summary" subsection in the
                        "Overview" section of a standard report.
asCode                  Format text as code.
asEmph                  Format text with emphasis (usually resulting in
                        text set in italics).
asFilename              Format text as filename.
asLink                  Format text as a hyperlink.
asParameter             Format text as parameter.
asReference             Reference a citation, figure or table element.
asStrong                Format text with strong emphasis (usually
                        resulting in text set in bold).
asSummary               Include a result in text. This is a legacy
                        method and provided only for backwards
                        compatibility.
asValue                 Format text as value.
getCollectionDate       Get the collection date of 'report'. The
                        collection date is the date when the collection
                        that this report is part of was created.
getCollectionVersion    Get the collection version of 'report'. The
                        collection version is the version string of the
                        collection that this report is part of.
getContactInformationEmail
                        Get contact email address for 'report'.
getContactInformationLabel
                        Get label for contact button for 'report'.
getContactInformationMessage
                        Get contact email default message for 'report'.
getContactInformationSubject
                        Get contact email subject line for 'report'.
getCopyrightOwner       Get name of the copyright owner for 'report'.
getCopyrightStatement   Get copyright statement for 'report'. This text
                        is linked to the copyright URL.
getCopyrightUrl         Get copyright URL for 'report', which is linked
                        to the copyright statement.
getCopyrightYear        Get copyright year 'report'.
getCreatorDate          Get date when 'report' was created.
getCreatorName          Get name and version of the Nozzle package that
                        was used to create 'report'.
getCustomPrintCss       Get the path or URL of the CSS file to be used
                        to overwrite the default print (not: screen)
                        style sheet.
getCustomScreenCss      Get the path or URL of the CSS file to be used
                        to overwrite the default screen (not: print)
                        style sheet.
getDoi                  Get the DOI (document object identifier,
                        http://www.doi.org) for 'report'.
getDoiCreator           Get the DOI creator for 'report'.
getDoiPublisher         Get the DOI publisher for 'report'.
getDoiResolver          Get the DOI resolver URL (e.g.
                        http://dx.doi.org) for 'report'.
getDoiTitle             Get the DOI title for 'report'.
getDoiVersion           Get the DOI version for 'report'.
getDoiYear              Get the DOI year for 'report'.
getExportedElement      Get an exported element from a report. This can
                        be used to generate aggregate reports. This is
                        an experimental feature of Nozzle and may not
                        lead to the expected results.
getExportedElementIds   Get the IDs of exported elements from 'report'.
                        This is an experimental feature of Nozzle and
                        may not lead to the expected results.
getFigureFile           Get path or URL of image file associated with a
                        figure element.
getFigureFileHighRes    Get path or URL of high-resolution of
                        vector-based image file associated with a
                        figure element.
getGoogleAnalyticsId    Get Google Analytics tracking ID for 'report'.
getLogo                 Get logo file for one of six positions (three
                        at the top, three at the bottom) in 'report'.
getMaintainerAffiliation
                        Get affiliation of maintainer of 'report'.
getMaintainerEmail      Get email address of maintainer of 'report'.
getMaintainerName       Get name of maintainer of 'report'.
getRendererDate         Get date when 'report' was rendered.
getRendererName         Get name and version of the Nozzle package that
                        was used to render 'report'.
getReportId             Get the ID (a UUID) of 'report'.
getReportSubTitle       Get the subtitle of 'report'.
getReportTitle          Get the title of 'report'.
getSignificantEntity    Get name of entities that are called out as
                        significant, e.g. "gene". This is currently not
                        being used and might become obsolete in future
                        versions of Nozzle.
getSignificantResultsCount
                        Get the total number of significant results in
                        'report'.
getSoftwareName         Get the name of the software that used Nozzle
                        to generate 'report'.
getSoftwareVersion      Get the version of the software that used
                        Nozzle to generate 'report'.
getSummary              Get the first element of the "Summary"
                        subsection in the "Overview" section in a
                        standard report.
getTableFile            Get path or URL of file associatd with table
                        element.
isFigure                Test if 'element' is a figure element.
isTable                 Test if 'element' is a table element.
newCitation             Create a citation element.
newCustomReport         Create a new custom report without pre-defined
                        sections.
newFigure               Create a new list element.
newHtml                 Create a new freeform HTML element. THIS MUST
                        BE USED WITH EXTRAORDINARTY CARE!
newJournalCitation      Create a citation element that represents a
                        document published in a journal. This is a
                        convenience wrapper for 'newCitation'.
newList                 Create a new list element.
newParagraph            Create a new paragraph element.
newParameterList        Create a new parameter list element. A
                        parameter list is an unnumbered list of the
                        form param_1 = value_1, ..., param_n = value_n
                        where param_i is formated as a parameter and
                        value_i is formatted as a value.
newReport               Create a new report with pre-defined sections
                        Overview/Introduction, Overview/Summary,
                        Results, Methods & Data/Input, Methods &
                        Data/References and Meta Data.
newResult               Create a new result element.
newSection              Create a new section element.
newSubSection           Create a new subsection element.
newSubSubSection        Create a new subsubsection element.
newTable                Create new table element.
newWebCitation          Create a citation element that represents a
                        document published online. This is a
                        convenience wrapper for 'newCitation'.
setCollectionDate       Set the collection date of 'report'. The
                        collection date is the date when the collection
                        that this report is part of was created.
setCollectionVersion    Set the collection version of 'report'. The
                        collection version is the version string of the
                        collection that this report is part of.
setContactInformation   Set contact information for 'report'. This is
                        used to create a "contact" button in the top
                        right corner of the report, e.g. to collect
                        feedback about the report.
setCopyright            Set copyright messsage for 'report'.
setCustomPrintCss       Set the path or URL of the CSS file to be used
                        to overwrite the default print (not: screen)
                        style sheet. Can be relative or absolute.
setCustomScreenCss      Set the path or URL of the CSS file to be used
                        to overwrite the default screen (not: print)
                        style sheet. Can be relative or absolute.
setDoi                  Set the DOI (document object identifier,
                        http://www.doi.org) for 'report'. A warning
                        will be emitted if the report has been assigned
                        a DOI before.
setDoiCreator           Set the DOI meta data creator for 'report'.
setDoiPublisher         Set the DOI meta data publisher for 'report'.
setDoiResolver          Set the DOI resolver URL (e.g.
                        http://dx.doi.org) for 'report'. The URL must
                        not end with a slash!
setDoiTitle             Set the DOI meta data title for 'report'.
setDoiVersion           Set the DOI meta data version for 'report'.
setDoiYear              Set the DOI meta data year for 'report'.
setFigureFile           Set path or URL of image file associated with a
                        figure element. Paths can relative or absolute.
setFigureFileHighRes    Set path or URL of high-resolution or
                        vector-based image file associated with a
                        figure element. Paths can be relative or
                        absolute.
setGoogleAnalyticsId    Set the Google Analytics tracking ID to be
                        embedded in this report ("web property id",
                        usually starts with "UA-").
setLogo                 Set a logo file for one of six positions (three
                        at the top, three at the bottom) in 'report',
                        e.g. an institute logo.
setMaintainerAffiliation
                        Set affiliation of maintainer of 'report'.
setMaintainerEmail      Set email address of maintainer of 'report'.
setMaintainerName       Set name of maintainer of 'report'.
setNextReport           Set the URL and title of the "next" report
                        after 'report'. This will be accessible through
                        the utility menu.
setParentReport         Set the URL and title of the "parent" report
                        above 'report'. This will be accessible through
                        the utility menu.
setPreviousReport       Set the URL and title of the "previous" report
                        before 'report'. This will be accessible
                        through the utility menu.
setReportSubTitle       Set the subtitle of 'report'.
setReportTitle          Set the title of 'report'.
setSignificantEntity    Set name of entities that are called out as
                        significant, e.g. "gene". This is currently not
                        being used and might become obsolete in future
                        versions of Nozzle.
setSoftwareName         Set the name of the software that used Nozzle
                        to generate 'report', e.g. "My Report Generator
                        Script".
setSoftwareVersion      Set the name of the software that used Nozzle
                        to generate 'report', e.g. "Version 1.2".
setTableFile            Set path or URL of file associatd with table
                        element.
writeReport             Write 'report' to file.
