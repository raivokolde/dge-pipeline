#! /bin/bash

rm -r Test
mkdir Test

mkdir Test/Reads

cp read_pairs.txt Test/
cp barcodes.txt Test/
cp samples_described.txt Test/ 

for file in $(cat Test/read_pairs.txt); do
    if [ ${file: -2} == "gz" ]
    then
        cat Reads/$file | gunzip | head -20000 | gzip -c > Test/Reads/$file
    else
        cat Reads/$file | head -20000 > Test/Reads/$file
    fi
done;

script_dir=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)

cd Test
python $script_dir/super_dge_pipeline.py --nolsf --organism $1

rm -r /home/unix/rkolde/public_html/Test
cp -r Report/ /home/unix/rkolde/public_html/Test

echo "http://www.broadinstitute.org/~rkolde/Test/"

