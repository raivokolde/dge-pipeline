import os, sys, re, numpy as np, argparse

def edger_to_rnk(in_file, out_file, gene_symbol_map):
	ranks = np.loadtxt(in_file, dtype='str', skiprows=1, usecols=[0,1], delimiter='\t')
	if gene_symbol_map is not None:
		for row in ranks:
			new_symbol = gene_symbol_map.get(row[0])
			if new_symbol is not None:
				row[0] = new_symbol
			else:
				# just convert to uppercase 
				row[0] = row[0].upper()
	np.savetxt(out_file, ranks, delimiter="\t",fmt="%s")

def get_gene_symbol_map(hom_file):
	gene_symbol_map = dict()
	with open(hom_file,'r') as hom:
		for line in hom:
			source, target = re.split('\t', line.strip())
			gene_symbol_map[source] = target
	return gene_symbol_map

def run(edger_dir, rnk_dir, hom_file):
	if hom_file is not None:
		gene_symbol_map = get_gene_symbol_map(hom_file)
	else:
		gene_symbol_map = None

	edger_files = filter(lambda x: x.endswith('.edgeR.tsv'), os.listdir(edger_dir))
	for edger_file in edger_files:
		base_name = edger_file.split('.edgeR.tsv')[0]
		edger_path = os.path.join(edger_dir, edger_file)
		rnk_path = os.path.join(rnk_dir, base_name+'.rnk')
		edger_to_rnk(edger_path, rnk_path, gene_symbol_map)

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("edger_dir")
	parser.add_argument("rnk_dir")
	parser.add_argument("--hom_file")
	args = parser.parse_args()

	run(args.edger_dir, args.rnk_dir, args.hom_file)

if __name__ == '__main__':
	main()