import os, sys

def get_significant_genes(edger_file, max_fdr):
	genes = dict()
	with open(edger_file,'r') as f:
		f.readline() # skip header
		for line in f:
			gene, logFC, logCPM, pvalue, fdr = line.strip().split()
			if float(fdr) <= max_fdr:
				genes[gene] = (logFC, logCPM, pvalue, fdr)

	return genes

def get_shared_genes(edger_files, output_file, max_fdr):
	# get all significant genes
	gene_dicts = list()
	for edger_file in edger_files:
		gene_dicts.append(get_significant_genes(edger_file, max_fdr))

	# find shared subset
	shared_genes = set(gene_dicts[0].keys())
	for other_dict in gene_dicts[1:]:
		shared_genes = shared_genes.intersection(set(other_dict.keys()))

	# only retain genes with consistent direction of DE
	# NB: remember that the direction depends on the order of samples!
	golden_genes = set()
	for gene in shared_genes:
		logFCs = [float(x[gene][0]) for x in gene_dicts]
		allup = reduce(lambda x,y: x and y, [(x > 0) for x in logFCs])
		alldown = reduce(lambda x,y: x and y, [(x < 0) for x in logFCs])
		if allup or alldown:
			golden_genes.add(gene)

	# write output
	with open(output_file,'w') as f:
		f.write('\t'.join(["Gene"]+["logFC_%s\tFDR_%s" % (x,x) for x in 
			map(os.path.basename, edger_files)])+'\n')
		for gene in golden_genes:
			f.write('\t'.join([gene]+[x[gene][0]+'\t'+x[gene][3] for x in gene_dicts])+'\n')

# main
max_fdr = 0.5
output_file = sys.argv[1]
input_files = sys.argv[2:]

get_shared_genes(input_files, output_file, max_fdr)