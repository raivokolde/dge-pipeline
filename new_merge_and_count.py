import os, sys, re, numpy as np, argparse

class SamRecord:
	def __init__(self, seq, qual, index, tag, umi, 
		ref_id, edit_dist, best_hits):
		self.seq = seq
		self.qual = qual
		self.index = index
		self.tag = tag
		self.umi = umi
		self.ref_id = ref_id
		self.edit_dist = edit_dist
		self.best_hits = best_hits

class CountMatrix:
	def __init__(self, genes, wells):
		self.genes = sorted(genes)
		self.wells = sorted(wells)
		self.gene_map = {gene: idx for idx, gene in enumerate(self.genes)}
		self.well_map = {well: idx for idx, well in enumerate(self.wells)}
		self.matrix = np.zeros((len(genes), len(wells)), np.int)

	def increment(self, gene, well):
		row = self.gene_map.get(gene, None)
		col = self.well_map.get(well, None)
		if (row is not None) and (col is not None):
			self.matrix[row,col] += 1

	def write(self, filename):
		with open(filename, "w") as f:
			f.write("\t".join([""] + self.wells)+"\n")
			for gene in self.genes:
				f.write("\t".join([gene] + [str(x) for x in 
					self.matrix[self.gene_map[gene], :]])+"\n")

class QcTable:
	"""
	A table for representing QC stats. 
	TODO this is bascially the same as the CountMatrix.
	We should probably just use a pandas DataFrame for both.
	"""
	def __init__(self, wells, categories):
		self.row_names = sorted(wells) + ["Unknown"]
		self.col_names = categories
		self.row_map = {row: idx for idx, row in enumerate(self.row_names)}
		self.col_map = {col: idx for idx, col in enumerate(self.col_names)}
		self.matrix = np.zeros((len(self.row_names), len(self.col_names)), np.int)
	
	def increment(self, well, category):
		row = self.row_map.get(well, self.row_map.get("Unknown"))
		col = self.col_map.get(category, None)
		if (row is not None) and (col is not None):
			self.matrix[row,col] += 1
	
	def write(self, filename, wells_to_conditions=None):
		with open(filename, "w") as f:
			header = "\t".join(["Well"] + self.col_names)
			if wells_to_conditions is not None:
				header += "\tCondition"
			f.write(header+"\n")
			for well in self.row_names:
				line = "\t".join([well] + [str(x) for x in 
					self.matrix[self.row_map[well], :]])
				if wells_to_conditions is not None:
					line += "\t"+wells_to_conditions.get(well,"None")
				f.write(line+"\n")

def extract_barcode_info(read_id):
	fields = read_id.strip().split(":")
	index, tag, umi = fields[-3], fields[-2], fields[-1]
	return index, tag, umi

def parse_sam_record(line):
	line = line.strip()
	if line.startswith("@"):
		return None
	fields = line.split()
	if len(fields) < 11:
		return None
	seq = fields[9]
	qual = fields[10]
	index, tag, umi = extract_barcode_info(fields[0])
	ref_id, edit_dist, best_hits = None, None, None
	if fields[2] != "*":
		ref_id = fields[2]
	if len(fields) >= 14:
		edit_dist = int(fields[12].split(":")[-1])
		best_hits = int(fields[13].split(":")[-1])
	return SamRecord(seq, qual, index, tag, umi, 
		ref_id, edit_dist, best_hits)

def smart_barcode_match(barcode_to_well, seq, max_edit_dist=1):
	# this is pretty slow
	well = barcode_to_well.get(seq, None)
	if well is not None:
		return well
	else:
		hits = list()
		for barcode in barcode_to_well:
			if edit_distance(barcode, seq) <= max_edit_dist:
				hits.append(barcode)
		if len(hits) == 1:
			return barcode_to_well.get(hits[0])
		else:
			return None

def edit_distance(seq1, seq2):
	return map(lambda x,y: x==y, seq1, seq2).count(False)

def make_barcode(tag, index):
	return tag + index

def unmake_barcode(barcode):
	tag, index = barcode[0:6], barcode[6:16]
	return tag, index

def get_refseq_mapping(sym2ref_file):
	# Read gene symbol->RefSeq ID mapping
	refseq_to_gene = dict()
	with open(sym2ref_file, "rU") as f:
		for line in f:
			items = line.split()
			gene, refseqs = items[0], items[1].split(",")
			refseq_to_gene.update([(rs, gene) for rs in refseqs])
	return refseq_to_gene

def get_barcode_mapping(barcode_file):
	# Read barcode->well mapping
	barcode_to_well = dict()
	with open(barcode_file, "rU") as f:
		for line in f:
			line = line.strip()
			if len(line) > 0:
				well, tag, index = line.split()
				combined_barcode = make_barcode(tag, index)
				barcode_to_well[combined_barcode] = well
	return barcode_to_well

def get_sample_mapping(samples_file):
	# Read well->condition mapping
	wells_to_conditions = dict()
	with open(samples_file, "ru") as f:
		for line in f:
			line = line.strip()
			if len(line) > 0:
				condition, well = line.split()
				wells_to_conditions[well] = condition
	return wells_to_conditions

def get_edits(base_seq, max_edit_dist=1):
	alphabet = ['A','C','G','T']
	edits = {base_seq}
	left = [base_seq] # being processed now
	right = [] # to be processed on next iter
	
	for ii in xrange(max_edit_dist):
		for seq in left:
			for jj in xrange(len(seq)):
				for letter in alphabet:
					edit_seq = seq[:jj]+letter+seq[jj+1:]
					if edit_seq not in edits:
						edits.add(edit_seq)
						right.append(edit_seq)
		left = right
		right = []
	return edits

def get_barcode_mapping_smart(barcode_file, max_edit_dist=1):
	# Same as get_barcode_mapping, but also maps barcodes with errors if possible
	# This allows us to avoid having to call smart_barcode_match for each entry
	# TODO this does not check whether edit distance is sufficient for uniqueness
	barcode_to_well_perfect = get_barcode_mapping(barcode_file)
	barcode_to_well_all = dict()

	for barcode in barcode_to_well_perfect:
		well = barcode_to_well_perfect[barcode]
		barcode_to_well_all[barcode] = well
		tag, index = unmake_barcode(barcode)
		for edited_tag in get_edits(tag, max_edit_dist=max_edit_dist):
			edited_barcode = make_barcode(edited_tag, index)
			barcode_to_well_all[edited_barcode] = well

	return barcode_to_well_all

def merge_and_count(refseq_to_gene, barcode_to_well, sam_list):

	# set constant parameters
	poly_tail_pattern = re.compile(r"A{10,}|T{10,}")
	max_edit_dist = 1
	max_best = 10

	# initialize data structures
	combined_umi_list = set()
	gene_list = set(refseq_to_gene.values())
	well_list = set(barcode_to_well.values())
	matrix_reads = CountMatrix(gene_list, well_list)
	matrix_umis = CountMatrix(gene_list, well_list)
	qc_cols = ["Total","Polytail","Aligned_All",
		"Aligned_Refseq","Aligned_Mito","Aligned_Other"]
	qc_reads = QcTable(well_list, qc_cols)
	qc_umis = QcTable(well_list, qc_cols)

	# convenience methods
	def update_matrices(gene, well, is_new_umi):
		matrix_reads.increment(gene, well)
		if is_new_umi:
			matrix_umis.increment(gene, well)

	def update_qc(well, category, is_new_umi):
		qc_reads.increment(well, category)
		if is_new_umi:
			qc_umis.increment(well, category)

	# main loop
	for sam_file in sam_list:
		with open(sam_file, "rU") as f:
			for line in f:
				sam = parse_sam_record(line)
				if sam is not None:
					# determine the identity (well + gene) of this read
					combined_barcode = make_barcode(sam.tag, sam.index)
					well = barcode_to_well.get(combined_barcode, None)
					gene = refseq_to_gene.get(sam.ref_id, None)

					# check the UMI and update the UMI list
					umi_components = [sam.umi, "NoWell", "NoRef"]
					if well is not None:
						umi_components[1] = well
					if gene is not None:
						umi_components[2] = gene
					elif sam.ref_id is not None:
						umi_components[2] = sam.ref_id

					combined_umi = "_".join(umi_components)
					is_new_umi = (combined_umi not in combined_umi_list)
					if is_new_umi:
						combined_umi_list.add(combined_umi)

					# further classify the read for counting and QC
					is_align_good = (sam.ref_id is not None
						and sam.edit_dist is not None
						and sam.best_hits is not None
						and sam.edit_dist <= max_edit_dist 
						and sam.best_hits <= max_best)
					is_assigned = (well is not None)
					is_refseq = (gene is not None)
					is_mito = (sam.ref_id is not None and sam.ref_id.startswith("chrM"))
					is_polytail = (re.search(poly_tail_pattern, sam.seq) is not None)

					# update expression matrices and QC
					update_qc(well, "Total", is_new_umi)
					if is_polytail:
						update_qc(well, "Polytail", is_new_umi)
					elif is_align_good:
						update_qc(well, "Aligned_All", is_new_umi)
						if is_mito:
							update_qc(well, "Aligned_Mito", is_new_umi)
						elif is_refseq:
							update_qc(well, "Aligned_Refseq", is_new_umi)
							update_matrices(gene, well, is_new_umi)
						else:
							update_qc(well, "Aligned_Other", is_new_umi)

	return matrix_reads, matrix_umis, qc_reads, qc_umis

def run(sym2ref_file, barcode_file, alignment_dir, counting_dir):
	refseq_to_gene = get_refseq_mapping(sym2ref_file)
	barcode_to_well = get_barcode_mapping_smart(barcode_file)

	sam_list = [os.path.join(alignment_dir, f) for f in os.listdir(alignment_dir) if f.endswith(".sam")]

	matrix_reads, matrix_umis, qc_reads, qc_umis = merge_and_count(refseq_to_gene, 
		barcode_to_well, sam_list)

	matrix_reads.write(os.path.join(counting_dir, "counts.total.dat"))
	matrix_umis.write(os.path.join(counting_dir, "counts.umi.dat"))
	qc_reads.write(os.path.join(counting_dir, "summary.total.dat"))
	qc_umis.write(os.path.join(counting_dir, "summary.umi.dat"))

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("sym2ref_file")
	parser.add_argument("barcode_file")
	parser.add_argument("alignment_dir")
	parser.add_argument("counting_dir")
	args = parser.parse_args()

	run(args.sym2ref_file, args.barcode_file, args.alignment_dir, args.counting_dir)

if __name__ == '__main__':
	main()

