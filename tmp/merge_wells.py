import os, sys, itertools, argparse

def merge_counts(wells, in_files, total_file, umi_file):
	# this assumes that all files have the same set of genes in the same order
	inputs = [open(f, "r") for f in in_files]
	zipped = itertools.izip(inputs)
	
	with open(total_file, "w") as tot, open(umi_file, "w") as umi:
		tot.write("\t".join([""]+wells))
		umi.write("\t".join([""]+wells))
		zipped.next() # skip input headers
		for lines_by_well in zipped:
			fields_by_well = [x.strip().split() for x in lines_by_well]
			gene_name = fields_by_well[0][0] # just take name from first well
			gene_totals = [x[1] for x in fields_by_well]
			gene_umis = [x[2] for x in fields_by_well]
			tot.write("\n" + "\t".join([gene_name] + gene_totals))
			umi.write("\n" + "\t".join([gene_name] + gene_umis))

	# clean up
	for f in inputs:
		f.close()

def merge_qc(wells, in_files, total_file, umi_file):
	# this is somewhat easier than merging counts - just need to concatenate inputs
	with open(total_file, "w") as tot, open(umi_file, "w") as umi:
		tot.write("\t".join(["Well","Total","Polytail","Aligned_All",
			"Aligned_Refseq","Aligned_Mito","Aligned_Other"]))
		umi.write("\t".join(["Well","Total","Polytail","Aligned_All",
			"Aligned_Refseq","Aligned_Mito","Aligned_Other"]))
		for ii in xrange(len(wells)):
			well_name = wells[ii]
			with open(in_files[ii], "r") as f:
				f.readline() # skip header
				tot_fields = f.readline().strip().split()[1:]
				umi_fields = f.readline().strip().split()[1:]
				tot.write("\n" + "\t".join([well_name] + tot_fields))
				umi.write("\n" + "\t".join([well_name] + umi_fields))

def get_wells(barcode_file):
	wells = list()
	with open(barcode_file, "r") as f:
		for line in f:
			line = line.strip()
			if len(line) > 0:
				wells.append(line.split()[0])
	return wells

def run(barcodes_file, unmerged_dir, counting_dir):
	wells = get_wells(barcodes_file)
	wells_plus_unknown = wells + ["Unknown"]
	count_files = [os.path.join(unmerged_dir, "%s.counts.dat" % x) for x in wells]
	summary_files = [os.path.join(unmerged_dir, "%s.summary.dat" % x) for x in wells_plus_unknown]
	total_counts = os.path.join(counting_dir, "counts.total.dat")
	umi_counts = os.path.join(counting_dir, "counts.umi.dat")
	total_summary = os.path.join(counting_dir, "summary.total.dat")
	umi_summary = os.path.join(counting_dir, "summary.umi.dat")

	merge_counts(wells, count_files, total_counts, umi_counts)
	merge_qc(wells_plus_unknown, summary_files, total_summary, umi_summary)

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("barcodes_file")
	parser.add_argument("unmerged_dir")
	parser.add_argument("counting_dir")
	args = parser.parse_args()

	run(args.barcodes_file, args.unmerged_dir, args.counting_dir)

if __name__ == '__main__':
	main()