import os, sys, re, argparse
from operator import add

class SamRecord:
	"""
	Container for SAM data.
	"""
	def __init__(self, seq, qual, index, tag, umi, 
		ref_id, edit_dist, best_hits):
		self.seq = seq
		self.qual = qual
		self.index = index
		self.tag = tag
		self.umi = umi
		self.ref_id = ref_id
		self.edit_dist = edit_dist
		self.best_hits = best_hits

class SamReader:
	"""
	Read the lines of a SAM file.
	"""
	def __init__(self, sam_path):
		self.sam = open(sam_path, "r")

	def __enter__(self):
		return self

	def __exit__(self, type, value, traceback):
		self.sam.close()

	def next(self):
		for line in self.sam:
			record = self.parse_sam_record(line)
			if record is not None:
				yield record

	@staticmethod
	def extract_barcode_info(read_id):
		fields = read_id.strip().split(":")
		index, tag, umi = fields[-3], fields[-2], fields[-1]
		return index, tag, umi

	@staticmethod
	def parse_sam_record(line):
		line = line.strip()
		if line.startswith("@"):
			return None
		fields = line.split()
		if len(fields) < 11:
			return None
		seq = fields[9]
		qual = fields[10]
		index, tag, umi = SamReader.extract_barcode_info(fields[0])
		ref_id, edit_dist, best_hits = None, None, None
		if fields[2] != "*":
			ref_id = fields[2]
		if len(fields) >= 14:
			edit_dist = int(fields[12].split(":")[-1])
			best_hits = int(fields[13].split(":")[-1])
		return SamRecord(seq, qual, index, tag, umi, 
			ref_id, edit_dist, best_hits)

def get_refseq_mapping(sym2ref_file):
	# Read gene symbol->RefSeq ID mapping
	refseq_to_gene = dict()
	with open(sym2ref_file, "rU") as f:
		for line in f:
			items = line.split()
			gene, refseqs = items[0], items[1].split(",")
			refseq_to_gene.update([(rs, gene) for rs in refseqs])
	return refseq_to_gene

def plus(l, inc):
	return map(add, l, inc)

def count_and_write(refseq_to_gene, sam_list, counts_out, qc_out):
	# set constant parameters
	poly_tail_pattern = re.compile(r"A{10,}|T{10,}")
	max_edit_dist = 1
	max_best = 10

	# create data structures + counters
	# NOTE: tuples represent (read count, umi count)
	umi_set = set()
	gene_counts = {x: (0,0) for x in refseq_to_gene.values()}
	total = (0,0)
	polytail = (0,0)
	aligned = (0,0)
	aligned_ref = (0,0)
	aligned_mito = (0,0)
	aligned_other = (0,0)

	for sam_file in sam_list:
		with SamReader(sam_file) as reader:
			for sam in reader.next():
				gene = refseq_to_gene.get(sam.ref_id, None)

				# classify the read for counting and QC
				is_aligned = (sam.ref_id is not None
					and sam.edit_dist is not None
					and sam.best_hits is not None
					and sam.edit_dist <= max_edit_dist 
					and sam.best_hits <= max_best)
				is_refseq = (gene is not None)
				is_mito = (sam.ref_id is not None and sam.ref_id.startswith("chrM"))
				is_polytail = (re.search(poly_tail_pattern, sam.seq) is not None)

				# check UMI
				genewise_umi = sam.umi
				if gene is not None:
					genewise_umi += gene
				elif sam.ref_id is not None:
					genewise_umi += sam.ref_id
				else:
					genewise_umi += "NoRef"

				if genewise_umi not in umi_set:
					umi_set.add(genewise_umi)
					inc = (1,1) # add to both total and umi
				else:
					inc = (1,0) # add to total only

				# do counting
				total = plus(total, inc)
				if is_polytail:
					polytail = plus(polytail, inc)
				elif is_aligned:
					aligned = plus(aligned, inc)
					if is_mito:
						aligned_mito = plus(aligned_mito, inc)
					elif is_refseq:
						aligned_ref = plus(aligned_ref, inc)
						gene_counts[gene] = plus(gene_counts[gene], inc)
					else:
						aligned_other = plus(aligned_other, inc)

	# write output
	with open(counts_out, "w") as f:
		f.write("\t".join(["","Total","UMI"]))
		for gene in sorted(gene_counts):
			f.write("\n" + "\t".join([gene, str(gene_counts[gene][0]), str(gene_counts[gene][1])]))
	with open(qc_out, "w") as f:
		f.write("\t".join(["","Total","Polytail","Aligned_All",
			"Aligned_Refseq","Aligned_Mito","Aligned_Other"]))
		f.write("\n" + "\t".join(["Total"]+[str(total[0]), str(polytail[0]), str(aligned[0]),
			str(aligned_ref[0]), str(aligned_mito[0]), str(aligned_other[0])]))
		f.write("\n" + "\t".join(["UMI"]+[str(total[1]), str(polytail[1]), str(aligned[1]),
			str(aligned_ref[1]), str(aligned_mito[1]), str(aligned_other[1])]))

def run(sym2ref_file, well, alignment_dir, unmerged_dir):
	refseq_to_gene = get_refseq_mapping(sym2ref_file)
	sam_list = [os.path.join(alignment_dir, f) for f in os.listdir(alignment_dir) \
		if f.startswith(well+".") and f.endswith(".sam")]
	counts_out = os.path.join(unmerged_dir, well+".counts.dat")
	qc_out = os.path.join(unmerged_dir, well+".summary.dat")
	count_and_write(refseq_to_gene, sam_list, counts_out, qc_out)

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("sym2ref_file")
	parser.add_argument("well")
	parser.add_argument("alignment_dir")
	parser.add_argument("unmerged_dir")
	args = parser.parse_args()

	run(args.sym2ref_file, args.well, args.alignment_dir, args.unmerged_dir)

if __name__ == '__main__':
	main()