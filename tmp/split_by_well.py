import os, sys, gzip, itertools, argparse

class MultiFastqReader:
	"""
	Reads FASTQ files in parallel.
	"""
	def __init__(self, *fastq_paths):
		self.fastqs = list()
		for f in fastq_paths:
			self.fastqs.append(self.open_fastq_or_gz(f))

	def __enter__(self):
		return self

	def __exit__(self, type, value, traceback):
		for f in self.fastqs:
			f.close()

	def next(self):
		zipped = itertools.izip(*self.fastqs)
		for headers in zipped:
			seqs = zipped.next()
			pluses = zipped.next()
			quals = zipped.next()

			clean = lambda x: x.strip()
			headers = map(clean, headers)
			seqs = map(clean, seqs)
			quals = map(clean, quals)

			yield (headers, seqs, quals)

	@staticmethod
	def open_fastq_or_gz(filename):
		if filename.endswith(".fastq") and os.access(filename, os.F_OK):
			return open(filename, "rU")
		elif filename.endswith(".fastq.gz") and os.access(filename, os.F_OK):
			return gzip.open(filename, "rb")
		elif filename.endswith(".fastq") and os.access(filename + ".gz", os.F_OK):
			return gzip.open(filename + ".gz", "rb")
		elif filename.endswith(".fastq.gz") and os.access(filename[:-3], os.F_OK):
			return open(filename[:-3], "rU")
		raise IOError, "Unknown file: " + filename

class FastqWriterByWell:
	"""
	Maintains a list of FASTQ handles, one per well/sample,
	subject to size constraints.
	"""
	def __init__(self, wells, out_dir, max_size=10000000):
		self.max_size = max_size
		self.out_dir = out_dir
		self.total_sizes = {w: 0 for w in wells}
		self.current_files = {w: open(self.make_filename(w), "w") for w in wells}

	def __enter__(self):
		return self

	def __exit__(self, type, value, traceback):
		for f in self.current_files.values():
			f.close()

	def write(self, well, header, seq, qual):
		if self.total_sizes[well] % self.max_size != 0:
			# if not beginning of file, start new line
			self.current_files[well].write("\n")
		self.current_files[well].write("\n".join([header, seq, "+", qual]))
		self.total_sizes[well] += 1
		if self.total_sizes[well] % self.max_size == 0:
			# start a new file for this well
			self.current_files[well].close()
			self.current_files[well] = open(self.make_filename(well), "w")

	def make_filename(self, well):
		return os.path.join(self.out_dir, "%s.%s.fastq" % (well, self.total_sizes[well]))

def get_tag_info(r1_seq, r1_qual, do_mask=True):
	if do_mask:
		tag = mask(r1_seq[0:6], r1_qual[0:6], min_qual=10)
		umi = mask(r1_seq[6:16], r1_qual[6:16], min_qual=30)
	else:
		tag = r1_seq[0:6]
		umi = r1_seq[6:16]
	return tag, umi

# Mask sequence by quality score
def mask(seq, qual, min_qual=30):
	return "".join((b if (ord(q) - 33) >= min_qual else "N") for b, q in itertools.izip(seq, qual))

def make_barcode(tag, index):
	return tag + index

def unmake_barcode(barcode):
	tag, index = barcode[0:6], barcode[6:16]
	return tag, index

def edit_distance(seq1, seq2):
	return map(lambda x,y: x==y, seq1, seq2).count(False)

def get_barcode_mapping(barcode_file):
	# Read barcode->well mapping
	barcode_to_well = dict()
	with open(barcode_file, "rU") as f:
		for line in f:
			line = line.strip()
			if len(line) > 0:
				well, tag, index = line.split()
				combined_barcode = make_barcode(tag, index)
				barcode_to_well[combined_barcode] = well
	return barcode_to_well

def get_barcode_mapping_smart(barcode_file, max_edit_dist=1):
	# Same as get_barcode_mapping, but also maps barcodes with errors if possible
	# This does NOT check whether edit distance is sufficient for uniqueness
	barcode_to_well_perfect = get_barcode_mapping(barcode_file)
	barcode_to_well_all = dict()

	for barcode in barcode_to_well_perfect:
		well = barcode_to_well_perfect[barcode]
		barcode_to_well_all[barcode] = well
		tag, index = unmake_barcode(barcode)
		for edited_tag in get_edits(tag, max_edit_dist=max_edit_dist):
			edited_barcode = make_barcode(edited_tag, index)
			barcode_to_well_all[edited_barcode] = well
	return barcode_to_well_all

def get_edits(base_seq, max_edit_dist=1):
	alphabet = ['A','C','G','T']
	edits = {base_seq}
	left = [base_seq] # being processed now
	right = [] # to be processed on next iter
	
	for ii in xrange(max_edit_dist):
		for seq in left:
			for jj in xrange(len(seq)):
				for letter in alphabet:
					edit_seq = seq[:jj]+letter+seq[jj+1:]
					if edit_seq not in edits:
						edits.add(edit_seq)
						right.append(edit_seq)
		left = right
		right = []
	return edits

def split_and_write(barcode_to_well, fastq_writer_by_well, r1_fastq, r2_fastq, idx_fastq=None):
	if idx_fastq is not None:
		input_files = [r1_fastq, r2_fastq, idx_fastq]
		has_sep_idx = True
	else:
		input_files = [r1_fastq, r2_fastq]
		has_sep_idx = False

	with MultiFastqReader(*input_files) as reader:
		for (headers, seqs, quals) in reader.next():
			tag, umi = get_tag_info(seqs[0], quals[0])
			if has_sep_idx:
				index = seqs[2]
			else:
				index = (headers[1].split()[1]).split(':')[3]
			new_head = ":".join([headers[1].split()[0], index, tag, umi])
			new_seq, new_qual = seqs[1], quals[1]

			well = barcode_to_well.get(make_barcode(tag, index), "Unknown")
			fastq_writer_by_well.write(well, new_head, new_seq, new_qual)

def run(read_pairs_file, barcodes_file, reads_dir, alignment_dir):

	barcode_to_well = get_barcode_mapping_smart(barcodes_file)
	well_labels = barcode_to_well.values() + ["Unknown"]

	with FastqWriterByWell(well_labels, alignment_dir) as fastq_writer_by_well:
		with open(read_pairs_file, "r") as sample_map:
			for line in sample_map:
				fields = line.split()
				r1_filename, r2_filename = fields[0], fields[1]
				r1_path = os.path.join(reads_dir, r1_filename)
				r2_path = os.path.join(reads_dir, r2_filename)
				if len(fields) > 2:
					idx_filename = fields[2]
					idx_path = os.path.join(reads_dir, idx_filename)
					split_and_write(barcode_to_well, fastq_writer_by_well, r1_path, r2_path, idx_path)
				else:
					split_and_write(barcode_to_well, fastq_writer_by_well, r1_path, r2_path)

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("read_pairs_file")
	parser.add_argument("barcodes_file")
	parser.add_argument("reads_dir")
	parser.add_argument("alignment_dir")
	args = parser.parse_args()

	run(args.read_pairs_file, args.barcodes_file, args.reads_dir, args.alignment_dir)

if __name__ == '__main__':
	main()
