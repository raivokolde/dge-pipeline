#!/usr/bin/env python

from __future__ import print_function
import os, sys, gzip, re, itertools, argparse, numpy as np
from Bio import SeqIO

def extract_barcode_info(read_id):
	fields = read_id.strip().split(":")
	index, tag, umi = fields[-3], fields[-2], fields[-1]
	return index, tag, umi

def parse_sam_record(line):
	line = line.strip()
	if line.startswith("@"):
		return None
	fields = line.split()
	if len(fields) < 11:
		return None
	seq = fields[9]
	qual = fields[10]
	index, tag, umi = extract_barcode_info(fields[0])
	ref_id, edit_dist, best_hits = None, None, None
	if fields[2] != "*":
		ref_id = fields[2]
	if len(fields) >= 14:
		edit_dist = int(fields[12].split(":")[-1])
		best_hits = int(fields[13].split(":")[-1])
	return SamRecord(seq, qual, index, tag, umi, 
		ref_id, edit_dist, best_hits)

def edit_distance(seq1, seq2):
	return map(lambda x,y: x==y, seq1, seq2).count(False)

def make_barcode(tag, index):
	return tag + index

def unmake_barcode(barcode):
	tag, index = barcode[0:6], barcode[6:16]
	return tag, index

def get_sample_mapping(samples_file):
	# Read well->condition mapping
	wells_to_conditions = dict()
	with open(samples_file, "ru") as f:
		for line in f:
			line = line.strip()
			if len(line) > 0:
				condition, well = line.split()
				wells_to_conditions[well] = condition
	return wells_to_conditions

def get_edits(base_seq, max_edit_dist=1):
	alphabet = ['A','C','G','T']
	edits = {base_seq}
	left = [base_seq] # being processed now
	right = [] # to be processed on next iter
	
	for ii in xrange(max_edit_dist):
		for seq in left:
			for jj in xrange(len(seq)):
				for letter in alphabet:
					edit_seq = seq[:jj]+letter+seq[jj+1:]
					if edit_seq not in edits:
						edits.add(edit_seq)
						right.append(edit_seq)
		left = right
		right = []
	return edits

def get_barcode_mapping(barcode_file):
	# Read barcode->well mapping
	barcode_to_well = dict()
	with open(barcode_file, "rU") as f:
		for line in f:
			line = line.strip()
			if len(line) > 0:
				well, tag, index = line.split()
				combined_barcode = make_barcode(tag, index)
				barcode_to_well[combined_barcode] = well
	return barcode_to_well

def get_barcode_mapping_smart(barcode_file, max_edit_dist=1):
	# Same as get_barcode_mapping, but also maps barcodes with errors if possible
	# This allows us to avoid having to call smart_barcode_match for each entry
	# TODO this does not check whether edit distance is sufficient for uniqueness
	barcode_to_well_perfect = get_barcode_mapping(barcode_file)
	barcode_to_well_all = dict()
	
	for barcode in barcode_to_well_perfect:
		well = barcode_to_well_perfect[barcode]
		barcode_to_well_all[barcode] = well
		tag, index = unmake_barcode(barcode)
		for edited_tag in get_edits(tag, max_edit_dist=max_edit_dist):
			edited_barcode = make_barcode(edited_tag, index)
			barcode_to_well_all[edited_barcode] = well
	
	return barcode_to_well_all

def main():
	base_dir = "."
	barcode_file = os.path.join(base_dir, "barcodes.txt")
	samples_file = os.path.join(base_dir, "samples_described.txt")
	read_dir = os.path.join(base_dir, "Alignment")

	dir_output = os.path.join(base_dir, "GEO")
	if not os.path.exists(dir_output):
		os.makedirs(dir_output)
	
	barcode_to_well = get_barcode_mapping_smart(barcode_file)
	wells = list(set(barcode_to_well.values()))
	
	output_con = dict()
	for well in wells:
		output_con[well] = open(os.path.join(dir_output, "%s.fastq"%(well)), "w")
	
	files = [x for x in os.listdir(read_dir) if re.search(".fastq$", x)]
	
	for file in files:
		file_iter = SeqIO.parse(os.path.join(read_dir, file), "fastq")
		
		i = 1
		for read in file_iter:
			if i % 1000000 == 0:
				print(i)
			
			index, tag, umi = extract_barcode_info(read.description)
			barcode = make_barcode(tag, index)
			well = barcode_to_well.get(barcode, None)
			
			if well == None:
				continue
			
			SeqIO.write(read, output_con[well], "fastq")
	
	for k in output_con.keys():
		output_con[k].close()

if __name__ == '__main__':
	main()








