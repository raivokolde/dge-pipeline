from __future__ import print_function
import os
import sys
import re
import numpy as np

def smart_barcode_match(barcode_to_well, seq, max_edit_dist=1):
    well = barcode_to_well.get(seq, None)
    if well is not None:
        return well
    else:
        hits = list()
        for barcode in barcode_to_well:
            if map(lambda x,y: x==y, barcode, seq).count(False) <= max_edit_dist:
                hits.append(barcode)
        if len(hits) == 1:
            return barcode_to_well.get(hits[0])
        else:
            return None

def merge_and_count(sym2ref, ercc_fasta, barcodes, aligned_dir, dge_dir):

    # Read gene symbol->RefSeq ID mapping
    gene_list = list()
    refseq_to_gene = dict()
    with open(sym2ref, "rU") as f:
        for line in f:
            items = line.split()
            gene, refseqs = items[0], items[1].split(",")
            gene_list.append(gene)
            refseq_to_gene.update([(rs, gene) for rs in refseqs])
    gene_to_idx = {gene: idx for idx, gene in enumerate(gene_list)}

    # Read ERCC control IDs
    ercc_list = list()
    with open(ercc_fasta, "rU") as f:
        for line in f:
            if line[0] == ">":
                items = line.split()
                ercc_list.append(items[0][1:])
    ercc_to_idx = {ercc: idx for idx, ercc in enumerate(ercc_list)}

    # Read barcode->well mapping
    well_list = list()
    barcode_to_well = dict()
    with open(barcodes, "rU") as f:
        for line in f:
            well, tag, index = line.split()
            well_list.append(well)
            combined_barcode = tag + index
            barcode_to_well[combined_barcode] = well

    # Create well to array index mapping
    well_to_idx = {well: idx for idx, well in enumerate(well_list)}

    # Initialize counters
    count_total_reads = 0
    count_assigned_reads_perfect = 0
    count_assigned_reads = 0
    count_aligned_reads = 0
    count_polytail_reads = 0
    count_accepted_reads = 0

    count_spike_total = np.zeros((len(ercc_list), len(well_to_idx)), np.int)
    count_spike_umi = np.zeros((len(ercc_list), len(well_to_idx)), np.int)

    count_total = np.zeros((len(gene_list), len(well_to_idx)), np.int)
    count_umi = np.zeros((len(gene_list), len(well_to_idx)), np.int)

    count_mito_reads = 0
    count_mito_umi = 0

    count_unknown_reads = 0
    count_unknown_umi = 0

    seen_umi = set()
    unknown = set()

    umi_breakdown = dict() # TEMP

    #accepted_barcode_pattern = re.compile(r"[ACGT]+$")
    poly_tail_pattern = re.compile(r"A{10,}|T{10,}")
    max_edit_dist = 1
    max_best = 10

    # Read each partial alignment file and add to counts
    for aligned_filename in [f for f in os.listdir(aligned_dir) if f.endswith(".sam")]:
        with open(os.path.join(aligned_dir, aligned_filename), "rU") as f:
            for line in f:
                if line.startswith("@"):
                    continue

                count_total_reads += 1

                items = line.split()
                read_id, aligned_id, read_seq = items[0], items[2], items[9]
                index, tag, umi = read_id.split(":")[-3], read_id.split(":")[-2], read_id.split(":")[-1]
                combined_barcode = tag + index

                # TEMP
                if umi not in umi_breakdown:
                    umi_breakdown[umi] = (set(), set(), set(), set())
                # END TEMP

                well = barcode_to_well.get(combined_barcode, None)
                if well is not None:
                    count_assigned_reads_perfect += 1
                else:
                    pass
                    # TEMP commenting this out
                    # well = smart_barcode_match(barcode_to_well, combined_barcode)

                is_assigned = (well is not None)
                is_aligned = (aligned_id != "*")
                is_polytail = (re.search(poly_tail_pattern, read_seq) is not None)

                if is_polytail:
                    count_polytail_reads += 1
                if is_aligned:
                    count_aligned_reads += 1
                if is_assigned:
                    count_assigned_reads += 1

                if (not is_assigned) or (not is_aligned) or is_polytail:
                    continue

                umi_breakdown[umi][0].add(well) # TEMP

                edit_dist, best_hits = int(items[12].split(":")[-1]), int(items[13].split(":")[-1])
                if (edit_dist > max_edit_dist) or (best_hits > max_best):
                    continue

                count_accepted_reads += 1

                if aligned_id.startswith("ERCC"):
                    umi = "_".join([index, tag, umi, aligned_id])
                    count_spike_total[ercc_to_idx[aligned_id], well_to_idx[well]] += 1
                    if umi not in seen_umi:
                        count_spike_umi[ercc_to_idx[aligned_id], well_to_idx[well]] += 1
                        seen_umi.add(umi)
                elif aligned_id.startswith("chrM"):

                    umi_breakdown[umi][2].add(aligned_id) # TEMP

                    umi = "_".join([index, tag, umi, aligned_id])
                    count_mito_reads += 1
                    if umi not in seen_umi:
                        count_mito_umi += 1
                        seen_umi.add(umi)
                elif aligned_id in refseq_to_gene:
                    gene = refseq_to_gene[aligned_id]

                    umi_breakdown[umi][1].add(gene) # TEMP

                    umi = "_".join([index, tag, umi, gene])
                    count_total[gene_to_idx[gene], well_to_idx[well]] += 1
                    if umi not in seen_umi:
                        count_umi[gene_to_idx[gene], well_to_idx[well]] += 1
                        seen_umi.add(umi)
                else:

                    umi_breakdown[umi][3].add(aligned_id) # TEMP

                    unknown.add(aligned_id)
                    umi = "_".join([index, tag, umi, aligned_id])
                    count_unknown_reads += 1
                    if umi not in seen_umi:
                        count_unknown_umi += 1
                        seen_umi.add(umi)


    # TEMP write UMI stats
    with open(os.path.join(dge_dir, "umi_breakdown.dat"), "w") as f:
        print("\t".join(["UMI", "Wells", "Genes", "Mitos", "Unknowns"]), file=f)
        for umi in umi_breakdown:
            print("\t".join([umi]+[str(len(x)) for x in umi_breakdown[umi]]), file=f)

    # Write summary of sample contents
    with open(os.path.join(dge_dir, "counts.log.dat"), "w") as f:
        print("\t".join(["Sample_ID",
                         "Total",
                         "Assigned_NoErr",
                         "Assigned_All",
                         "Aligned",
                         "PolyAT_Tail",
                         "Accepted",
                         "Spike_Total",
                         "Spike_UMI",
                         "Mito_Total",
                         "Mito_UMI",
                         "Refseq_Total",
                         "Refseq_UMI",
                         "Unknown_Total",
                         "Unknown_UMI"]),
                file=f)
        print("\t".join(["null"] +
                         [str(x) for x in [count_total_reads,
                                            count_assigned_reads_perfect,
                                            count_assigned_reads,
                                            count_aligned_reads,
                                            count_polytail_reads,
                                            count_accepted_reads,
                                            count_spike_total.sum(),
                                            count_spike_umi.sum(),
                                            count_mito_reads,
                                            count_mito_umi,
                                            count_total.sum(),
                                            count_umi.sum(),
                                            count_unknown_reads,
                                            count_unknown_umi]]),
                file=f)

    # Write list of RefSeq IDs that were not mapped to genes by UCSC
    with open(os.path.join(dge_dir, "counts.unknown_list"), "w") as f:
        for name in unknown:
            print(name, file=f)

    # Write gene x well total alignment counts
    with open(os.path.join(dge_dir, "counts.total.dat"), "w") as f:
        print("\t".join([""] + well_list), file=f)
        for gene in gene_list:
            print("\t".join([gene] +
                             [str(x) for x in count_total[gene_to_idx[gene], :]]),
                    file=f)

    # Write gene x well UMI counts
    with open(os.path.join(dge_dir, "counts.umi.dat"), "w") as f:
        print("\t".join([""] + well_list), file=f)
        for gene in gene_list:
            print("\t".join([gene] + \
                             [str(x) for x in count_umi[gene_to_idx[gene], :]]), \
                    file=f)

    # Write ERCC x well total alignment counts
    with open(os.path.join(dge_dir, "spikes.total.dat"), "w") as f:
        print("\t".join([""] + well_list), file=f)
        for ercc in ercc_list:
            print("\t".join([ercc] + \
                             [str(x) for x in count_spike_total[ercc_to_idx[ercc], :]]), \
                    file=f)

    # Write ERCC x well UMI counts
    with open(os.path.join(dge_dir, "spikes.umi.dat"), "w") as f:
        print("\t".join([""] + well_list), file=f)
        for ercc in ercc_list:
            print("\t".join([ercc] + \
                             [str(x) for x in count_spike_umi[ercc_to_idx[ercc], :]]), \
                    file=f)

    # Write by-well total and UMI counts
    with open(os.path.join(dge_dir, "well_summary.dat"), "w") as f:
        print("\t".join([""] + well_list), file=f)
        print("\t".join(["Refseq_Total"] +[str(x) for x in count_total.sum(axis=0)]), file=f)
        print("\t".join(["Refseq_UMI"] +[str(x) for x in count_umi.sum(axis=0)]), file=f)
        print("\t".join(["Spike_Total"] +[str(x) for x in count_spike_total.sum(axis=0)]), file=f)
        print("\t".join(["Spike_UMI"] +[str(x) for x in count_spike_umi.sum(axis=0)]), file=f)

def run_merge_and_count(species, barcodes, alignment_dir, dge_dir):
    refseq_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Reference"
    script_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts"

    sym2ref = {"Human": os.path.join(refseq_dir, "Human_RefSeq", "refGene.hg19.sym2ref.dat"), 
        "Mouse": os.path.join(refseq_dir, "Mouse_RefSeq", "refGene.mm10.sym2ref.dat"),
        "Rat": os.path.join(refseq_dir, "Rat_RefSeq", "refGene.rn5.sym2ref.dat")}
    sym2ref = sym2ref[species]

    ercc_fasta = os.path.join(refseq_dir, "ERCC92.fa")

    merge_and_count(sym2ref, ercc_fasta, barcodes, alignment_dir, dge_dir)

