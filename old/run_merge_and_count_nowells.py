from __future__ import print_function
import os
import sys

## Parameters

bsub_queue = "hour -W 240"
bsub_resreq = "rusage[mem=4000]"

sym2ref = {"Human": os.path.join("Reference", "Human_RefSeq", "refGene.hg19.sym2ref.dat"),
            "Mouse": os.path.join("Reference", "Mouse_RefSeq", "refGene.mm10.sym2ref.dat")}

ercc_fasta = os.path.join("Reference", "ERCC92.fa")

## Helpers

# Contruct LSF bsub command for merge_and_count job
def bsub_mc_cmd(sample_id, sym2ref, ercc_fasta, alignment_dir, dge_dir):
    return " ".join(["bsub", "-q", bsub_queue, "-J merge_and_count", "-o", os.path.join(dge_dir, "merge_and_count.bsub"),
                        "-R", "\"" + bsub_resreq + "\"", "\"python Scripts/merge_and_count_nowells.py", 
                        sample_id, sym2ref, ercc_fasta, alignment_dir, dge_dir + "\""])

## Main

# Parse command line parameters
if len(sys.argv) != 5:
    print("Usage: python " + sys.argv[0] + " <sample map> Human|Mouse <alignment dir> <dge_dir>", file=sys.stderr)
    sys.exit()

sample_map, species, alignment_dir, dge_dir = sys.argv[1:]
sym2ref = sym2ref[species]

# Read though sample map and launch merge_and_count jobs for each sample_id
with open(sample_map, "rU") as sm_file:
    for sample_id in set([line.split()[0] for line in sm_file]):
        os.system(bsub_mc_cmd(sample_id, sym2ref, ercc_fasta, alignment_dir, dge_dir))
