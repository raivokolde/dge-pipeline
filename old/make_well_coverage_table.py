well_summary_file = "Counting/well_summary.dat"
samples_file = "samples_described.txt"
combined_file = "sample_summary.txt"

with open(well_summary_file,'r') as well_summary:
	wells = well_summary.readline().strip().split('\t')
	totals = well_summary.readline().strip().split('\t')[1:]
	umis = well_summary.readline().strip().split('\t')[1:]

assert len(wells) == len(totals)
assert len(wells) == len(umis)

total_map = dict(zip(wells,totals))
umi_map = dict(zip(wells,umis))

with open(samples_file,'r') as sample_desc, open(combined_file,'w') as sample_summary:
	for line in sample_desc:
		line = line.strip().split('\t')
		if len(line) < 2:
			# preserve empty lines
			sample_summary.write('\n')
		else:
			well = line[1]
			sample_summary.write('\t'.join(line+[total_map.get(well,-1), umi_map.get(well, -1)]) + '\n')