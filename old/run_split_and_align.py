from __future__ import print_function
import os
import os.path
import sys

## Parameters

bwa_started_file = "started"
bwa_finished_file = "finished"
bwa_launch_marker = "--ALL JOBS LAUNCHED--"

script_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts"

bsub_queue = "hour -W 240"
bsub_memreq = "\"rusage[mem=4000]\""
bsub_ioreq = "\"rusage[sulfur_io=3]\""

reference_prefix_map = {"Human": os.path.join("Reference", "Human_RefSeq", "refMrna_ERCC_polyAstrip.hg19.fa"),
                        "Mouse": os.path.join("Reference", "Mouse_RefSeq", "refMrna_ERCC_polyAstrip.mm10.fa"),
			"Rat": os.path.join("Reference", "Rat_RefSeq", "refMrna_ERCC_polyAstrip.rn5.fa")}

## Helpers

# Contruct LSF bsub command for split_and_align job
def bsub_sa_cmd(sample_id, subsample_id, r1_path, r2_path, alignment_dir, reference_prefix):
    return " ".join(["bsub", "-q", bsub_queue, "-J split_and_align", "-o", os.path.join(alignment_dir, "split_and_align.bsub"), \
                        "-R", bsub_memreq, "-R", bsub_ioreq, "\"python", os.path.join(script_dir, "split_and_align.py"), sample_id, \
                        subsample_id, r1_path, r2_path, alignment_dir, reference_prefix + "\""])

# same as above, but run locally
def local_sa_cmd(sample_id, subsample_id, r1_path, r2_path, alignment_dir, reference_prefix):
    return " ".join(["python", os.path.join(script_dir, "split_and_align.py"), sample_id, \
                        subsample_id, r1_path, r2_path, alignment_dir, reference_prefix])

## Main

# Parse command line parameters
if len(sys.argv) != 4:
    print("Usage: python " + sys.argv[0] + " <sample map> Human|Mouse <alignment dir>", file=sys.stderr)
    sys.exit()

sample_map_filename, species, alignment_dir = sys.argv[1:]
reference_prefix = reference_prefix_map[species]

# Read though sample map and launch split_and_align jobs for each entry
with open(sample_map_filename, "rU") as sample_map:
    for line in sample_map:
        sample_id, subsample_id, r1_filename, r2_filename = line.split()
        r1_path = os.path.join(os.path.dirname(sample_map_filename), r1_filename)
        r2_path = os.path.join(os.path.dirname(sample_map_filename), r2_filename)
        #os.system(bsub_sa_cmd(sample_id, subsample_id, r1_path, r2_path, alignment_dir, reference_prefix))
        os.system(local_sa_cmd(sample_id, subsample_id, r1_path, r2_path, alignment_dir, reference_prefix))

# confirm that all align jobs have been launched
os.system(" ".join(["echo", bwa_launch_marker, ">>", os.path.join(alignment_dir, bwa_started_file)]))



