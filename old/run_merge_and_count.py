from __future__ import print_function
import os
import sys

## Parameters

refseq_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Reference"
script_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts"

bsub_queue = "hour -W 240"
bsub_memreq = "\"rusage[mem=8000]\""
bsub_ioreq = "\"rusage[sulfur_io=3]\""

sym2ref = {"Human": os.path.join(refseq_dir, "Human_RefSeq", "refGene.hg19.sym2ref.dat"),
            "Mouse": os.path.join(refseq_dir, "Mouse_RefSeq", "refGene.mm10.sym2ref.dat"),
		"Rat": os.path.join(refseq_dir, "Rat_RefSeq", "refGene.rn5.sym2ref.dat")}

ercc_fasta = os.path.join(refseq_dir, "ERCC92.fa")

## Helpers

# Contruct LSF bsub command for merge_and_count job
def bsub_mc_cmd(sample_id, sym2ref, ercc_fasta, barcodes, alignment_dir, dge_dir):
    return " ".join(["bsub", "-q", bsub_queue, "-J merge_and_count", "-o", os.path.join(dge_dir, "merge_and_count.bsub"),
                        "-R", bsub_memreq, "-R", bsub_ioreq, "\"python", os.path.join(script_dir, "merge_and_count.py"),
                        sample_id, sym2ref, ercc_fasta, barcodes, alignment_dir, dge_dir + "\""])

# same as bsub_mc_cmd, but run locally
def local_mc_cmd(sym2ref, ercc_fasta, barcodes, alignment_dir, dge_dir):
    return " ".join(["python", os.path.join(script_dir, "merge_and_count.py"), sym2ref, ercc_fasta, barcodes, 
        alignment_dir, dge_dir])

## Main

# Parse command line parameters
if len(sys.argv) != 5:
    print("Usage: python " + sys.argv[0] + " Human|Mouse <barcodes> <alignment dir> <dge_dir>", file=sys.stderr)
    sys.exit()

species, barcodes, alignment_dir, dge_dir = sys.argv[1:]
sym2ref = sym2ref[species]

os.system(local_mc_cmd(sym2ref, ercc_fasta, barcodes, alignment_dir, dge_dir))
