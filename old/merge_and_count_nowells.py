from __future__ import print_function
import os
import sys
import re
import numpy as np

## Main

if len(sys.argv) != 6:
    print("Usage: python " + sys.argv[0] + " <sample id> <sym2ref file> <ERCC fasta> <alignment dir> <dge dir>",
            file=sys.stderr)
    sys.exit()

sample_id, sym2ref, ercc_fasta, aligned_dir, dge_dir = sys.argv[1:]

# Read gene symbol->RefSeq ID mapping
gene_list = list()
refseq_to_gene = dict()
with open(sym2ref, "rU") as f:
    for line in f:
        items = line.split()
        gene, refseqs = items[0], items[1].split(",")
        gene_list.append(gene)
        refseq_to_gene.update([(rs, gene) for rs in refseqs])
gene_to_idx = {gene: idx for idx, gene in enumerate(gene_list)}

# Read ERCC control IDs
ercc_list = list()
with open(ercc_fasta, "rU") as f:
    for line in f:
        if line[0] == ">":
            items = line.split()
            ercc_list.append(items[0][1:])
ercc_to_idx = {ercc: idx for idx, ercc in enumerate(ercc_list)}

# Initialize counters
count_total_reads = 0
count_assigned_reads = 0
count_assigned_aligned_reads = 0

count_spike_total = np.zeros((len(ercc_list), 1), np.int)
count_spike_umi = np.zeros((len(ercc_list), 1), np.int)

count_total = np.zeros((len(gene_list), 1), np.int)
count_umi = np.zeros((len(gene_list), 1), np.int)

count_assigned_unknown_reads = 0
count_assigned_unknown_umi = 0

seen_umi = set()
unknown = set()

accepted_barcode_pattern = re.compile(r"[ACGT]+[ACG]$")
polyA_tail_pattern = re.compile(r"A{20,}$")
max_edit_dist = 1
max_best = 10

# Read each partial alignment file and add to counts
for aligned_filename in [f for f in os.listdir(aligned_dir)
                        if f.startswith(sample_id + ".") and f.endswith(".sam")]:
    with open(os.path.join(aligned_dir, aligned_filename), "rU") as f:
        for line in f:
            if line.startswith("@"):
                continue
            count_total_reads += 1
            items = line.split()
            read_id, aligned_id = items[0], items[2]
            barcode = read_id.split(":")[-1]
            if not re.match(accepted_barcode_pattern, barcode):
                continue
            count_assigned_reads += 1
            if aligned_id == "*":
                continue
            read, edit_dist, best_hits = items[9], int(items[12].split(":")[-1]), int(items[13].split(":")[-1])
            if (edit_dist > max_edit_dist) or (best_hits > max_best) or (re.search(polyA_tail_pattern, read)):
                continue
            count_assigned_aligned_reads += 1
            if aligned_id.startswith("ERCC"):
                umi = "_".join([barcode, aligned_id])
                count_spike_total[ercc_to_idx[aligned_id]] += 1
                if umi not in seen_umi:
                    count_spike_umi[ercc_to_idx[aligned_id]] += 1
                    seen_umi.add(umi)
            elif aligned_id in refseq_to_gene:
                gene = refseq_to_gene[aligned_id]
                umi = "_".join([barcode, gene])
                count_total[gene_to_idx[gene]] += 1
                if umi not in seen_umi:
                    count_umi[gene_to_idx[gene]] += 1
                    seen_umi.add(umi)
            else:
                unknown.add(aligned_id)
                umi = "_".join([barcode, aligned_id])
                count_assigned_unknown_reads += 1
                if umi not in seen_umi:
                    count_assigned_unknown_umi += 1
                    seen_umi.add(umi)

# Write summary of sample contents
with open(os.path.join(dge_dir, sample_id + ".log.dat"), "w") as f:
    print("\t".join(["Sample_ID",
                     "Total",
                     "Assigned",
                     "Aligned",
                     "Spike_Total",
                     "Spike_UMI",
                     "Refseq_Total",
                     "Refseq_UMI",
                     "Unknown_Total",
                     "Unknown_UMI"]),
            file=f)
    print("\t".join([sample_id] +
                     [str(x) for x in [count_total_reads,
                                        count_assigned_reads,
                                        count_assigned_aligned_reads,
                                        count_spike_total.sum(),
                                        count_spike_umi.sum(),
                                        count_total.sum(),
                                        count_umi.sum(),
                                        count_assigned_unknown_reads,
                                        count_assigned_unknown_umi]]),
            file=f)

# Write list of RefSeq IDs that were not mapped to genes by UCSC
with open(os.path.join(dge_dir, sample_id + ".unknown_list"), "w") as f:
    for name in unknown:
        print(name, file=f)

# Write gene x well total alignment counts
with open(os.path.join(dge_dir, sample_id + ".refseq.total.dat"), "w") as f:
    for gene in gene_list:
        print("\t".join([gene, str(count_total[gene_to_idx[gene], 0])]),
                file=f)

# Write gene x well UMI counts
with open(os.path.join(dge_dir, sample_id + ".refseq.umi.dat"), "w") as f:
    for gene in gene_list:
        print("\t".join([gene, str(count_umi[gene_to_idx[gene], 0])]), \
                file=f)

# Write ERCC x well total alignment counts
with open(os.path.join(dge_dir, sample_id + ".spike.total.dat"), "w") as f:
    for ercc in ercc_list:
        print("\t".join([ercc, str(count_spike_total[ercc_to_idx[ercc], 0])]), \
                file=f)

# Write ERCC x well UMI counts
with open(os.path.join(dge_dir, sample_id + ".spike.umi.dat"), "w") as f:
    for ercc in ercc_list:
        print("\t".join([ercc, str(count_spike_umi[ercc_to_idx[ercc], 0])]), \
                file=f)
