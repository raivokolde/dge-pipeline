import os, sys, re, numpy as np

def edger_to_rnk(in_file, out_file, gene_symbol_map):
	ranks = np.loadtxt(in_file, dtype='str', skiprows=1, usecols=[0,1], delimiter='\t')
	if gene_symbol_map is not None:
		for row in ranks:
			new_symbol = gene_symbol_map.get(row[0])
			if new_symbol is not None:
				row[0] = new_symbol
			else:
				# just convert to uppercase 
				row[0] = row[0].upper()
	np.savetxt(out_file, ranks, delimiter="\t",fmt="%s")

def get_gene_symbol_map(hom_file):
	gene_symbol_map = dict()
	with open(hom_file,'r') as hom:
		for line in hom:
			source, target = re.split('\t', line.strip())
			gene_symbol_map[source] = target
	return gene_symbol_map

def flip_comparison(a_vs_b):
	a, b = re.split('_vs_', a_vs_b)
	return b+'_vs_'+a

def sanitize_comparisons(in_file, out_file, rnk_dir):
	"""
	Make sure comparison ordering agrees with rank files.
	E.g. if we have SAMPLE_1_vs_SAMPLE_2.rnk, but 
	SAMPLE_2_vs_SAMPLE_1 is listed in the comparisons file, 
	we need to flip the latter.
	"""
	ranks = map(lambda x: x.rstrip('.rnk'), filter(lambda x: x.endswith('.rnk'), os.listdir(rnk_dir)))
	correct_comparisons = list()
	with open(in_file,'r') as comp_in:
		for line in comp_in:
			line = line.strip()
			if line in ranks:
				correct_comparisons.append(line)
			elif flip_comparison(line) in ranks:
				correct_comparisons.append(flip_comparison(line))
	with open(out_file,'w') as comp_out:
		for line in correct_comparisons:
			comp_out.write(line+"\n")

def run(counts_file, comparisons_in, comparisons_out, edger_dir, rnk_dir, hom_file=None):
	# get gene symbol conversion
	if hom_file is not None:
		gene_symbol_map = get_gene_symbol_map(hom_file)
	else:
		gene_symbol_map = None

	# make RNK files from EdgeR results
	counts_base = os.path.basename(counts_file)
	pat = re.compile('(?<='+re.escape(counts_base)+'\.)'+'.*'+'(?=\.edgeR.DE_results)')
	for results_file in filter(
		lambda x: x.endswith('.edgeR.DE_results') or x.endswith('.edgeR.DE_results.tsv'), 
		os.listdir(edger_dir)):
		m = re.search(pat, results_file)
		if m is not None:
			results_file = os.path.join(edger_dir, results_file)
			rnk_file = os.path.join(rnk_dir, m.group(0)+'.rnk')
			edger_to_rnk(results_file, rnk_file, gene_symbol_map)

	# rearrange comparisons
	sanitize_comparisons(comparisons_in, comparisons_out, rnk_dir)

