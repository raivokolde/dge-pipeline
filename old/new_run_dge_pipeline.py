#!/usr/bin/env python

import os, sys, re, shutil, argparse
import split_samples, new_merge_and_count, lsf_map, preprocess_gsea, validate_inputs

# General methods
# ------------------------------------------------------------------------------------------

def soft_mkdir(mydir):
	if not os.path.isdir(mydir):
		os.mkdir(mydir)

def files_for_organism(organism):
	base_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Reference/"
	if organism.lower() == 'mouse':
		refseq_file = base_dir + "Mouse_RefSeq/refMrna_ERCC_polyAstrip.mm10.fa"
		sym2ref_file = base_dir + "Mouse_RefSeq/refGene.mm10.sym2ref.dat"
		hom_file = base_dir + "mouse_to_human"
	elif organism.lower() == 'human':
		refseq_file = base_dir + "Human_RefSeq/refMrna_ERCC_polyAstrip.hg19.fa"
		sym2ref_file = base_dir + "Human_RefSeq/refGene.hg19.sym2ref.dat"
		hom_file = None
	else:
		raise Exception("Unknown species")

	return refseq_file, sym2ref_file, hom_file

# Command generation
# ------------------------------------------------------------------------------------------

def make_bwa_cmd(fastq_path, reference_prefix, alignment_dir):
	bwa = "/broad/software/free/Linux/redhat_5_x86_64/pkgs/bwa_0.7.4/bwa"
	aln = "aln -l 24 %s %s" % (reference_prefix, fastq_path)
	samse = "samse %s - %s > %s.sam" % (reference_prefix, fastq_path, fastq_path)
	cmd = " ".join([bwa, aln, "|", bwa, samse])
	return cmd

def make_edger_cmd(counts_file, samples_file, out_dir):
	base = "/seq/regev_genome_portal/SOFTWARE/TrinityDevel/" \
		+ "Analysis/DifferentialExpression/run_DE_analysis.pl"
	global_pars = " ".join(["--method edgeR"])
	input_pars = "--matrix %s --samples_file %s" % (counts_file, samples_file)
	output_pars = "--output %s" % out_dir
	log_pars = "> edgeR.log"
	cmd = " ".join([base, global_pars, input_pars, output_pars, log_pars])
	return cmd

def make_gsea_cmd(rank_file, gmt_file, out_dir):
	java = "java -cp"
	jar = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/" \
		+ "Scripts/java/gsea2-2.1.0.jar"
	memreq = "-Xmx1G"
	module = "xtools.gsea.GseaPreranked"
	global_pars = " ".join(["-collapse false",
							"-set_min 15",
							"-set_max 500",
							"-scoring_scheme weighted",
							"-nperm 1000",
							"-norm meandiv",
							"-mode Max_probe",
							"-include_only_symbols true",
							"-rnd_seed timestamp"
						])
	input_pars = "-gmx %s -rnk %s" % (gmt_file, rank_file)
	output_file = "_".join([os.path.join(out_dir, os.path.basename(rank_file)).rstrip(".rnk"), 
		os.path.basename(gmt_file), "output"])
	output_pars = "-out %s" % output_file 
	#log_pars = "> %s" % os.path.join(out_dir, "gsea.log")
	cmd = " ".join([java, jar, memreq, module, global_pars, input_pars, output_pars])
	return cmd

# Pipeline steps
# ------------------------------------------------------------------------------------------

def run_validate(read_pairs_file, barcodes_file, 
		samples_described_file, samples_compared_file, reads_dir):
	validate_inputs.run(read_pairs_file, barcodes_file, 
		samples_described_file, samples_compared_file, reads_dir)

def run_split(read_pairs_file, reads_dir, alignment_dir, sep_idx=False):
	split_samples.run(read_pairs_file, reads_dir, alignment_dir, separate_idx_file=sep_idx)

def run_bwa(reference_prefix, alignment_dir):
	cmd_array = []
	for fastq in filter(lambda x: x.endswith('.fastq'), os.listdir(alignment_dir)):
		fastq_path = os.path.join(alignment_dir,fastq)
		cmd_array.append(make_bwa_cmd(fastq_path, reference_prefix, alignment_dir))
	lsf_map.execute(cmd_array, alignment_dir, memreq="4000")

def run_count(sym2ref_file, barcode_file, samples_file, alignment_dir, counting_dir):
	new_merge_and_count.run(sym2ref_file, barcode_file, alignment_dir, counting_dir, 
		samples_file=samples_file)

def run_count_qc():
	os.system("Rscript /broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/counts2quantiles.R .")
	os.system("Rscript /broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/qc_analysis.R .")

def run_edger(counts_file, samples_file, edger_dir):
	# make sure output directory does not exist (to avoid crash)
	if os.path.isdir(edger_dir):
		shutil.rmtree(edger_dir)
	cmd = make_edger_cmd(counts_file, samples_file, edger_dir)
	os.system(cmd)

def run_prep_gsea(counts_file, comparisons_in, comparisons_out, edger_dir, rnk_dir, hom_file):
	preprocess_gsea.run(counts_file, comparisons_in, comparisons_out, edger_dir, rnk_dir, 
		hom_file=hom_file)

def run_gsea(comparisons_file, gmt_files, rnk_dir, gsea_dir):
	rnk_files = []
	with open(comparisons_file,'r') as comps:
		rnk_files  = [os.path.join(rnk_dir, comp.strip()+".rnk") for comp in comps.readlines()]
	cmd_array = []
	for rnk_file in rnk_files:
		for gmt_file in gmt_files:
			cmd_array.append(make_gsea_cmd(rnk_file, gmt_file, gsea_dir))
	lsf_map.execute(cmd_array, gsea_dir, memreq="1000")

# Main method
# ------------------------------------------------------------------------------------------

def main():

	# hard params
	organism = "mouse" # mouse or human
	separate_idx_file = False # True if we're using separate index files
	start_step = 0
	end_step = 99
	steps = range(start_step, end_step+1)
	# can also run specific subset, e.g.
	# steps = [1,2,5]

	# TODO add argparsing

	# directories
	reads_dir = "Reads"
	alignment_dir = "Alignment"
	counting_dir = "Counting"
	edger_dir = "EdgeR"
	gsea_dir = "GSEA"
	rnk_dir = "GSEA/Ranks"

	soft_mkdir(alignment_dir)
	soft_mkdir(counting_dir)
	#soft_mkdir(edger_dir) <- EdgeR will die if this dir exists beforehand
	soft_mkdir(gsea_dir)
	soft_mkdir(rnk_dir)

	# input files
	read_pairs = "read_pairs.txt"
	samples_described = "samples_described.txt"
	samples_compared = "samples_compared.txt"
	samples_compared_rearranged = "samples_compared_rearranged.txt"
	barcodes = "barcodes.txt"

	# organism-specific files
	refseq_file, sym2ref_file, hom_file = files_for_organism(organism)
	
	# etc files
	gene_sets = ["c5.bp.gmt", "c5.cc.gmt", "c5.mf.gmt"]
	gmt_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/gmt"
	gmt_files = [os.path.join(gmt_dir, g) for g in gene_sets]
	counts_file = os.path.join(counting_dir, "counts.umi.dat")

	# do stuff
	# TODO something less hacky?
	if 0 in steps:
		run_validate(read_pairs, barcodes, samples_described, samples_compared, reads_dir)
	if 1 in steps:
		run_split(read_pairs, reads_dir, alignment_dir, sep_idx=separate_idx_file)
	if 2 in steps:
		run_bwa(refseq_file, alignment_dir)
	if 3 in steps:
		run_count(sym2ref_file, barcodes, samples_described, alignment_dir, counting_dir)
	if 4 in steps:
		run_count_qc()
	if 5 in steps:
		run_edger(counts_file, samples_described, edger_dir)
	if 6 in steps:
		run_prep_gsea(counts_file, samples_compared, samples_compared_rearranged, 
			edger_dir, rnk_dir, hom_file=hom_file)
	if 7 in steps:
		run_gsea(samples_compared_rearranged, gmt_files, rnk_dir, gsea_dir)

if __name__ == '__main__':
	main()
