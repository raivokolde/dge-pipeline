import sys
import re
import numpy as np

in_file, out_file, organism = sys.argv[1:]

hom_file = '/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/misc/mouse_to_human'

ranks = np.loadtxt(in_file, dtype='str', skiprows=1, usecols=[0,1], delimiter='\t')

# GSEA will only read human gene symbols
if organism.lower() == 'mouse':
	mouse_to_human = dict()
	with open(hom_file,'r') as hom:
		for line in hom:
			mouse, human = re.split('\t', line.strip())
			mouse_to_human[mouse] = human
	for row in ranks:
		new_symbol = mouse_to_human.get(row[0])
		if new_symbol is not None:
			row[0] = new_symbol
		else:
			# just convert to uppercase (handles majority of mouse->human conversions)
			row[0] = row[0].upper()

np.savetxt(out_file, ranks, delimiter="\t",fmt="%s")