#!/bin/bash -l

# This wrapper script is necessary because:
# 1. The dotkit-accessible GenePattern libraries are only compatible with older (pre-3.0) versions of R
# 2. Other analysis steps (i.e. EdgeR) require at least R 3.0

#reuse -q .GenePattern-1.0.2-r-2.13.1

/broad/software/free/Linux/redhat_5_x86_64/pkgs/r_2.13.1/bin/Rscript --vanilla /broad/xavierlab_datadeposit/kostic/GMB/L1000/RunGPGSEA.R $@

#reuse -q R-3.0