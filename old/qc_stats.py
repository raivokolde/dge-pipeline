import os, sys, gzip, itertools, numpy, re
from collections import OrderedDict

'''
this is a temporary hack for QC
'''

def open_fastq_or_gz(filename):
	if filename.endswith(".fastq") and os.access(filename, os.F_OK):
		return open(filename, "rU")
	elif filename.endswith(".fastq.gz") and os.access(filename, os.F_OK):
		return gzip.open(filename, "rb")
	elif filename.endswith(".fastq") and os.access(filename + ".gz", os.F_OK):
		return gzip.open(filename + ".gz", "rb")
	elif filename.endswith(".fastq.gz") and os.access(filename[:-3], os.F_OK):
		return open(filename[:-3], "rU")
	raise IOError, "Unknown file: " + filename

def extract_tag_and_umi(seq):
	tag = seq[0:6]
	umi = seq[6:16]
	return (tag, umi)

def mask(seq, qual, min_qual=30):
	return "".join((b if (ord(q) - 33) >= min_qual else "N") for b, q in itertools.izip(seq, qual))

def is_high_quality(seq, qual, min_qual=30):
	return (min([ord(q) - 33 for q in qual]) >= min_qual)

def has_tail(seq, min_length=10):
	poly_tail_pattern = re.compile(r"A{"+str(min_length)+",}|T{"+str(min_length)+",}")
	return (re.search(poly_tail_pattern, seq) is not None)

def make_barcode(tag, index):
	return tag+index

def read_barcodes(barcode_file):
	well_list = list()
	barcode_to_well = dict()
	with open(barcode_file, "rU") as f:
		for line in f:
			well, tag, index = line.split()
			well_list.append(well)
			combined_barcode = make_barcode(tag, index)
			barcode_to_well[combined_barcode] = well
	return well_list, barcode_to_well

def analyze_wells(barcode_file, r1_fastq_list):

	well_list, barcode_map = read_barcodes(barcode_file)

	well_totals = OrderedDict([(w, 0) for w in well_list])
	well_goods = OrderedDict([(w, 0) for w in well_list])

	well_totals["Unknown"] = 0
	well_goods["Unknown"] = 0

	for r1_fastq in r1_fastq_list:
		with open_fastq_or_gz(r1_fastq) as r1:
			for header in r1:
				seq, plus, qual = r1.next(), r1.next(), r1.next()
				index = (header.split()[1]).split(':')[3]
				tag, tag_qual = seq[0:6], qual[0:6]
				tag_is_hq = is_high_quality(tag, tag_qual, min_qual=10)
				barcode = make_barcode(tag, index)
				well = barcode_map.get(barcode, "Unknown")
				well_totals[well] += 1
				if tag_is_hq:
					well_goods[well] += 1

	cols = ['Well','Total','Good']
	print '\t'.join(cols)
	for well in well_totals:
		print '\t'.join([well, str(well_totals[well]), str(well_goods[well])])

def count_seqs(r1_fastq, r2_fastq):
	tot_counts = 0
	hq_counts = 0
	tail_counts = 0
	hq_notail_counts = 0
	tag_table = dict()
	umi_table = dict()
	with open_fastq_or_gz(r1_fastq) as r1_file, open_fastq_or_gz(r2_fastq) as r2_file:
		r1_r2 = itertools.izip(r1_file, r2_file)
		for header1, header2 in r1_r2:
			seq1, seq2 = r1_r2.next()
			plus1, plus2 = r1_r2.next()
			qual1, qual2 = r1_r2.next()
			tag, umi = extract_tag_and_umi(seq1)
			tag_table[tag] = tag_table.get(tag,0) + 1
			umi_table[umi] = umi_table.get(umi,0) + 1
			is_hq = is_high_quality(seq1, qual1)
			is_tail = has_tail(seq2)

			tot_counts += 1
			if is_hq:
				hq_counts += 1
			if is_tail:
				tail_counts += 1
			if (is_hq and not is_tail):
				hq_notail_counts += 1

	return [tot_counts, hq_counts, tail_counts, hq_notail_counts, len(tag_table), len(umi_table)]

def stats_header():
	return '\t'.join(["Name", "Total", "HQ", "Tail", "HQ+noTail", "Tags", "UMIs"])

def stats_table(label, r1_fastq, r2_fastq):
	stats = count_seqs(r1_fastq, r2_fastq)
	return '\t'.join([label]+[str(s) for s in stats])

def get_nth_fastq_chunk(fastq_dir, n):
	# n must be less than 10 for now...
	all_files = os.listdir(fastq_dir)
	r1_fastq = filter(lambda x: x.endswith("R1_00"+str(n)+".fastq.gz"), all_files)
	r2_fastq = filter(lambda x: x.endswith("R2_00"+str(n)+".fastq.gz"), all_files)
	if (len(r1_fastq) > 0 and len(r2_fastq) > 0):
		return os.path.join(fastq_dir,r1_fastq[0]), os.path.join(fastq_dir,r2_fastq[0])
	else:
		return None

def print_stats(fastq_dir):
	print "Processing "+fastq_dir
	num_chunks = 1
	curr_chunk = get_nth_fastq_chunk(fastq_dir, num_chunks)
	print stats_header()
	while curr_chunk is not None:
		#print "Processing chunk "+str(num_chunks)
		label = "Chunk"+str(num_chunks)
		print stats_table(label, curr_chunk[0], curr_chunk[1])
		num_chunks += 1
		curr_chunk = get_nth_fastq_chunk(fastq_dir, num_chunks)

def print_stats_per_file(fastq_dir):
	print "Processing "+fastq_dir
	print stats_header()
	for r1_fastq in [os.path.join(fastq_dir,f) for f in os.listdir(fastq_dir) if f.endswith("_R1_001.fastq.gz")]:
		prefix = r1_fastq.split("_R1_001.fastq.gz")[0]
		r2_fastq = prefix + "_R2_001.fastq.gz"
		label = prefix.split("/")[-1]
		print stats_table(label, r1_fastq, r2_fastq)

def auto_well_analysis():
	barcode_file = 'barcodes.txt'
	reads_dir = 'Reads'
	read_pairs_file = 'read_pairs.txt'
	r1_fastq_list = list()

	with open(read_pairs_file, "rU") as sample_map:
		for line in sample_map:
			r1_filename, r2_filename = line.split()
			r1_path = os.path.join(reads_dir, r1_filename)
			r1_fastq_list.append(r1_path)

	analyze_wells(barcode_file, r1_fastq_list)


def count_seqs_simple(r1_fastq):
	tot_counts = 0
	umi_set = set()
	with open_fastq_or_gz(r1_fastq) as r1:
		for header in r1:
			seq, plus, qual = r1.next(), r1.next(), r1.next()
			tag, umi = extract_tag_and_umi(seq)
			tot_counts += 1
			umi_set.add(umi)
	umi_counts = len(umi_set)
	return tot_counts, umi_counts

#indexed_dir = "/broad/xavierlab_datadeposit/doconnell/MiSeq_Data/140605_M02867_0007_000000000-A8FVR/Unaligned/Project_DGE_QC/"
#unindexed_dir = "/broad/xavierlab_datadeposit/doconnell/MiSeq_Data/140605_M02867_0007_000000000-A8FVR/Unaligned/Undetermined_indices/"
#dir_dir = "/broad/xavierlab_datadeposit/doconnell/MiSeq_Data/140205_MS2010941-50V2_BMDC_3DGE_5K_BMDC_tBHQ_KEAP1"

#print_stats_per_file(dir_dir)

#for run_dir in os.listdir(indexed_dir):
#	print_stats(os.path.join(indexed_dir,run_dir))
#for run_dir in os.listdir(unindexed_dir):
#	print_stats(os.path.join(unindexed_dir,run_dir))

#auto_well_analysis()

r1_list = [
		"KL/Reads/KL1_S1_L001_R1_001.fastq.gz",
		"KL/Reads/KL2_S2_L001_R1_001.fastq.gz",
		"TM/Reads/TM1_S3_L001_R1_001.fastq.gz",
		"TM/Reads/TM2_S4_L001_R1_001.fastq.gz",
		"TM/Reads/TM3_S5_L001_R1_001.fastq.gz"
		]

for r1_fastq in r1_list:
	tot, umi = count_seqs_simple(r1_fastq)
	print "\t".join([r1_fastq,str(tot),str(umi)])

