qfrom __future__ import print_function, division, unicode_literals
import sys
import re
from Bio import SeqIO


## Main

if len(sys.argv) not in [2, 3]:
    print("Usage: python " + sys.argv[0] + " <fasta> [max num of A] [min length]", 
            file=sys.stderr)
    sys.exit()
    
fasta_filename = sys.argv[1]
max_A = sys.argv[2] if len(sys.argv) >= 3 else 5
min_len = sys.argv[3] if len(sys.argv) >= 4 else 300

polyA_pattern = re.compile(r"(A{" + str(max_A) + r",})$")

with open(fasta_filename, "rU") as f:
    for record in SeqIO.parse(f, "fasta"):
        record.seq = record.seq.upper()
        if re.search(polyA_pattern, str(record.seq)):
            record.seq = record.seq.rstrip("A") + "A" * max_A
        if len(record.seq) >= min_len:
            SeqIO.write(record, sys.stdout, "fasta")
