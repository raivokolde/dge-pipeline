import sys
import re

out_file = sys.argv[1]
in_files = sys.argv[2:]

out_table = open(out_file,'w')
in_tables = [open(in_file,'r') for in_file in in_files]

# write combined header (make sure column names are unique)
platenum = 0
for in_table in in_tables:
	platenum += 1
	for colname in re.split('\t',in_table.readline().strip()):
		plate_pos = re.split('_', colname, maxsplit=1)[1]
		out_table.write('\tP'+str(platenum)+'_'+plate_pos)

# write each gene (assume the order is the same for each input file)
while True:
	first = True
	out_line = '\n'
	for in_table in in_tables:
		in_line = in_table.readline()
		if in_line == '': # we've reached EOF
			out_table.close()
			for in_table in in_tables:
				in_table.close()
			sys.exit()
		else:
			gene, counts = re.split('\t',in_line.strip(),maxsplit=1)
			if first:
				out_line += gene
				first = False
			out_line += ('\t'+counts)
	out_table.write(out_line)
	

