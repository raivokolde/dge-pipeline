#!/usr/bin/env python
import os, sys, re, time, subprocess, shutil
import split_samples, align_samples, merge_and_count

# global parameters
organism = "Mouse"
gene_sets = "c5.bp.gmt,c5.cc.gmt,c5.mf.gmt"

combined_counts = "counts.umi.dat"

sample_map = "read_pairs.txt"
samples_described = "samples_described.txt"
samples_compared = "samples_compared.txt"
barcodes = "barcodes.txt"

refseq_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Reference"
script_dir = "/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts"

# directory setup
def soft_mkdir(mydir):
	if not os.path.isdir(mydir):
		os.mkdir(mydir)

read_dir = "Reads"
alignment_dir = "Alignment"
counting_dir = "Counting"
edger_dir = "EdgeR"
gsea_dir = "GSEA"
rnk_dir = "GSEA/Ranks"

soft_mkdir(alignment_dir)
soft_mkdir(counting_dir)
#soft_mkdir(edger_dir) <- EdgeR will die if this dir exists beforehand
soft_mkdir(gsea_dir)
soft_mkdir(rnk_dir)

# pipeline steps

def run_BWA():

	def bwa_finished(bwa_log):
		successful_jobs = 0
		if os.path.isfile(bwa_log):
			with open(bwa_log, 'r') as log:
				for line in log:
					if "Successfully completed" in line:
						successful_jobs += 1

		fastq_files = filter(lambda x: x.endswith('.fastq'), os.listdir(alignment_dir))
		sam_files = filter(lambda x: x.endswith('.fastq.sam'), os.listdir(alignment_dir))

		return (successful_jobs == len(fastq_files) and len(sam_files) == len(fastq_files))

	print "Starting alignment"
	
	bwa_log = os.path.join(alignment_dir, "bwa.bsub")
	# need to remove existing BWA log
	if os.path.isfile(bwa_log):
		os.remove(bwa_log)

	split_samples.run(sample_map, read_dir, alignment_dir)
	align_samples.run_align(organism, refseq_dir, alignment_dir)

	while not bwa_finished(bwa_log):
		time.sleep(60)

	print "All alignments done"

def run_counting():

	merge_and_count.run_merge_and_count(organism, barcodes, alignment_dir, counting_dir)

def run_edgeR():

	def run_edgeR_better(counts_file, samples_file, out_dir):
		base = "/seq/regev_genome_portal/SOFTWARE/TrinityDevel/" \
			+ "Analysis/DifferentialExpression/run_DE_analysis.pl"
		global_pars = " ".join(["--method edgeR"])
		input_pars = "--matrix %s --samples_file %s" % (counts_file, samples_file)
		output_pars = "--output %s" % out_dir

		cmd = " ".join([base, global_pars, input_pars, output_pars])
		os.system(cmd)

	# make sure output dir doesn't exist (else EdgeR will die)
	if os.path.isdir(edger_dir):
		shutil.rmtree(edger_dir)

	print "Starting EdgeR"

	edgeR = "/seq/regev_genome_portal/SOFTWARE/TrinityDevel/Analysis/DifferentialExpression/run_DE_analysis.pl"
	cmd = " ".join([edgeR, "--matrix", os.path.join(counting_dir, combined_counts), 
		"--method", "edgeR", "--samples_file", samples_described, "--output", edger_dir, 
		">", "edgeR.log"])
	os.system(cmd)
	os.system("mv edgeR.log "+os.path.join(edger_dir,"edgeR.log"))

def run_GSEA():

	samples_compared_rearranged = samples_compared+"_rearranged.txt"

	def run_gsea_local(rank_file, gmt_file, out_dir):
		base = "java -cp "+os.path.join(script_dir,"java","gsea2-2.1.0.jar")
		memreq = "-Xmx 1G"
		module = "xtools.gsea.GseaPreranked"
		global_pars = " ".join(["-collapse false",
								"-set_min 15",
								"-set_max 500",
								"-scoring_scheme weighted",
								"-nperm 1000",
								"-norm meandiv",
								"-mode Max_probe",
								"-include_only_symbols true",
								"-rnd_seed timestamp"
							])

		input_pars = "-gmx %s -rnk %s" % (gmt_file, rank_file)
		output_file = "_".join([os.path.join(out_dir, os.path.basename(rank_file)).rstrip(".rnk"), 
			gmt_file, "output.zip"])
		output_pars = "-file %s" % output_file 

		cmd = " ".join([base, memreq, module, global_pars, input_pars, output_pars])
		os.system(cmd)

	def run_gsea_rscript():
		gsea = os.path.join(script_dir, "run_legacy_gpgsea.sh")
		cmd = " ".join([gsea, samples_compared_rearranged, rnk_dir, gsea_dir, gene_sets, 
			">", os.path.join(gsea_dir, "gsea.log")])
		os.system(cmd)

	def run_make_rnk_all():
		""" Make .rnk files for GSEA """
		make_rnk = os.path.join(script_dir, "make_rnk.py")
		pat = re.compile('(?<='+re.escape(combined_counts)+'\.)'+'.*'+'(?=\.edgeR.DE_results)')
		for results_file in filter(
			lambda x: x.endswith('.edgeR.DE_results') or x.endswith('.edgeR.DE_results.tsv'), 
			os.listdir(edger_dir)):

			m = re.search(pat, results_file)
			if m is not None:
				rnk_file = m.group(0) + '.rnk'
				cmd = " ".join(["python", make_rnk, os.path.join(edger_dir, results_file), 
					os.path.join(rnk_dir, rnk_file), organism])
				os.system(cmd)

	def flip_comparison(a_vs_b):
		a, b = re.split('_vs_', a_vs_b)
		return b+'_vs_'+a

	def sanitize_comparisons():
		"""
		Make sure comparison ordering agrees with rank files.
		E.g. if we have SAMPLE_1_vs_SAMPLE_2.rnk, but 
		SAMPLE_2_vs_SAMPLE_1 is listed in the comparisons file, 
		we need to flip the latter.
		"""
		ranks = map(lambda x: x.rstrip('.rnk'), filter(lambda x: x.endswith('.rnk'), os.listdir(rnk_dir)))
		with open(samples_compared,'r') as comp_in, open(samples_compared_rearranged,'w') as comp_out:
				for line in comp_in:
					line = line.strip()
					if line in ranks:
						comp_out.write(line+'\n')
					elif flip_comparison(line) in ranks:
						comp_out.write(flip_comparison(line)+'\n')

	# TODO this doesn't extract to the right place
	def extract_results():	
		for zip_dir in filter(lambda x: x.endswith('.zip'), os.listdir(gsea_dir)):
			os.system("unzip -n "+os.path.join(gsea_dir,zip_dir))+" -d "+os.path.join(gsea_dir,zip_dir.rstrip('.zip'))

	print "Starting GSEA"

	run_make_rnk_all()
	sanitize_comparisons()
	run_gsea_rscript()
	#extract_results()

# main
#run_BWA()
run_counting()
#run_edgeR()
#run_GSEA()
