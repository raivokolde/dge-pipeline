from __future__ import print_function
import os, sys, gzip, itertools

def open_fastq_or_gz(filename):
	if filename.endswith(".fastq") and os.access(filename, os.F_OK):
		return open(filename, "rU")
	elif filename.endswith(".fastq.gz") and os.access(filename, os.F_OK):
		return gzip.open(filename, "rb")
	elif filename.endswith(".fastq") and os.access(filename + ".gz", os.F_OK):
		return gzip.open(filename + ".gz", "rb")
	elif filename.endswith(".fastq.gz") and os.access(filename[:-3], os.F_OK):
		return open(filename[:-3], "rU")
	raise IOError, "Unknown file: " + filename

# Mask sequence by quality score
def mask(seq, qual, min_qual=30):
	return "".join((b if (ord(q) - 33) >= min_qual else "N") for b, q in itertools.izip(seq, qual))

# Extract sample tag + UMI from read 1
def get_tag_info(r1_seq, r1_qual, do_mask=True):
	if do_mask:
		tag = mask(r1_seq[0:6], r1_qual[0:6], min_qual=10)
		umi = mask(r1_seq[6:16], r1_qual[6:16], min_qual=30)
	else:
		tag = r1_seq[0:6]
		umi = r1_seq[6:16]
	return tag, umi

# split input into chunks, then write them to alignment_dir
def split_and_write(r1_fastq, r2_fastq, alignment_dir, batch_size=10000000):
	with open_fastq_or_gz(r1_fastq) as r1_file, open_fastq_or_gz(r2_fastq) as r2_file:
		read_count = 0
		buf = list()
		base_name = os.path.basename(r2_fastq).rstrip('.gz').rstrip('.fastq')
		r1_r2 = itertools.izip(r1_file, r2_file)
		for header1, header2 in r1_r2:
			seq1, seq2 = r1_r2.next()
			plus1, plus2 = r1_r2.next()
			qual1, qual2 = r1_r2.next()

			read_name_1, read_name_2 = header1.split()[0][1:], header2.split()[0][1:]

			if read_name_1 != read_name_2:
			   continue

			index = (header2.split()[1]).split(':')[3]
			seq2, qual2 = seq2.rstrip(), qual2.rstrip()
			tag, umi = get_tag_info(seq1, qual1)
			seq, qual = seq2, qual2
			barcoded_name = ":".join([read_name_2, index, tag, umi])

			buf.append((barcoded_name, seq, qual))
			read_count += 1
			if read_count % batch_size == 0:
				write_fastq(alignment_dir, base_name, read_count, buf)
				buf = list()

		# write remainder chunk
		if len(buf) > 0:
			write_fastq(alignment_dir, base_name, read_count, buf)

def split_and_write_separate_index(r1_fastq, r2_fastq, idx_fastq, 
		alignment_dir, batch_size=10000000):
	with open_fastq_or_gz(r1_fastq) as r1_file, open_fastq_or_gz(r2_fastq) as r2_file, \
		open_fastq_or_gz(idx_fastq) as idx_file:

		read_count = 0
		buf = list()
		base_name = os.path.basename(r2_fastq).rstrip('.gz').rstrip('.fastq')
		r1_r2_idx = itertools.izip(r1_file, r2_file, idx_file)
		for header1, header2, headerX in r1_r2_idx:
			seq1, seq2, index = r1_r2_idx.next()
			plus1, plus2, plusX = r1_r2_idx.next()
			qual1, qual2, qualX = r1_r2_idx.next()

			read_name_1, read_name_2 = header1.split()[0][1:], header2.split()[0][1:]

			if read_name_1 != read_name_2:
			   continue

			seq2, qual2 = seq2.rstrip(), qual2.rstrip()
			index = index.strip()
			tag, umi = get_tag_info(seq1, qual1)
			seq, qual = seq2, qual2
			barcoded_name = ":".join([read_name_2, index, tag, umi])

			buf.append((barcoded_name, seq, qual))
			read_count += 1
			if read_count % batch_size == 0:
				write_fastq(alignment_dir, base_name, read_count, buf)
				buf = list()

		# write remainder chunk
		if len(buf) > 0:
			write_fastq(alignment_dir, base_name, read_count, buf)

def write_fastq(alignment_dir, base_name, read_count, name_seq_qual_list):
	fastq_path = os.path.join(alignment_dir, ".".join([base_name, str(read_count), "fastq"]))
	with open(fastq_path, "w") as out:
		for name, seq, qual in name_seq_qual_list:
			print("\n".join(["@" + name, seq, "+", qual]), file=out)

def run(read_pairs_file, reads_dir, alignment_dir, separate_idx_file=False):
	# Read though pairs and launch split jobs for each entry
	with open(read_pairs_file, "rU") as sample_map:
		for line in sample_map:
			fields = line.split()
			r1_filename, r2_filename = fields[0], fields[1]
			r1_path = os.path.join(reads_dir, r1_filename)
			r2_path = os.path.join(reads_dir, r2_filename)
			if separate_idx_file:
				idx_filename = fields[2]
				idx_path = os.path.join(reads_dir, idx_filename)
				split_and_write_separate_index(r1_path, r2_path, idx_path, alignment_dir)
			else:
				split_and_write(r1_path, r2_path, alignment_dir)

