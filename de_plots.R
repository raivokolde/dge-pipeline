script_dir = dirname(regmatches(commandArgs(), regexpr("(?<=^--file=).+", commandArgs(), perl=TRUE)))
library(ggplot2, lib.loc=file.path(script_dir,'/R/lib'))
library(plyr, lib.loc=file.path(script_dir,'/R/lib'))
library(stringr, lib.loc=file.path(script_dir,'/R/lib'))
library(magrittr, lib.loc=file.path(script_dir,'/R/lib'))
library(gProfileR, lib.loc=file.path(script_dir,'/R/lib'))
library(GOsummaries, lib.loc=file.path(script_dir,'/R/lib'))
library(stringr, lib.loc=file.path(script_dir,'/R/lib'))

read_data = function(edger_files, input_dir){
    res = list()
    for(x in edger_files){
        print(x)
        tab = read.delim(file.path(input_dir, x))
        res[[x]] = data.frame(Gene = rownames(tab), tab)
    }
    
    res = ldply(res)
    
    colnames(res)[1] = "Comparison"
    
    return(res)
}



extract_gene_list = function(edger_files){
    gl = dlply(edger_results, "Comparison", function(x){
        list(
            Down = x$Gene[x$FDR < 0.05 & x$logFC < 0] %>% as.character,
            Up = x$Gene[x$FDR < 0.05 & x$logFC > 0] %>% as.character
        )
    })
    
    names(gl) = str_replace(edger_files, ".edgeR.tsv", "")
    
    return(gl)
}

create_gosummaries = function(edger_files, organism, ...){
    organism = switch(organism, mouse = "mmusculus", human = "hsapiens")
    
    gl = extract_gene_list(edger_files)
    
    gs = gosummaries(gl, organism = organism, max_p_value = 0.05, ...)
    
    return(gs)
}



plot_gosummaries = function(edger_files, organism, filename){
    gs = create_gosummaries(edger_files, organism)
    
    plot(gs, filename = filename)
    
    return(gs)
}

gprofiler_tables = function(edger_files, organism, output_dir){ 
    # Format gene list
    gl = extract_gene_list(edger_files)
    
    query = list()
    for(comp in names(gl)){
        for(dir in c("Down", "Up")){
            x = gl[[comp]][[dir]]
            if(length(x) == 0){
                x = c("uugabuuga", "uugabuuga2")
            }
            
            name = str_c(comp, dir, sep = "_")
            query[[name]] = x
        }
    }
    
    # Run gProfiler
    gp = gprofiler(query, organism)
    
    # Create output directory
    dir.create(output_dir, showWarnings=FALSE)
    
    # Write summary file of the counts
    counts = matrix(NA, nrow = length(edger_files), ncol = 2, dimnames = list(names(gl), c("Down", "Up")))
    
    for(comp in rownames(counts)){
        for(dir in colnames(counts)){
            name = str_c(comp, dir, sep = "_")
            counts[comp, dir] = nrow(gp[gp$query.number == name, ])
        }
    }
    
    write.csv(counts, file.path(output_dir, "counts.csv"))
    
    # Create result files
    d_ply(gp, "query.number", function(x){
        filename = str_c(x[1, "query.number"], ".csv")
        path = file.path(output_dir, filename)
        res = x[order(x$p.value), c("domain", "term.name", "p.value", "term.size", "query.size", "overlap.size", "intersection")]
        res$intersection = str_replace_all(res$intersection, ",", ", ")
        
        write.csv(res, path)
    })
    
}

args = commandArgs(TRUE)
base_dir = args[1]
organism = args[2]

organism = switch(organism, mouse = "mmusculus", human = "hsapiens")

input_dir = file.path(base_dir, "EdgeR")
output_dir = file.path(base_dir, "Report")

dir.create(output_dir, showWarnings=FALSE)

edger_files = list.files(input_dir, pattern = ".edgeR.tsv")

edger_results = read_data(edger_files, input_dir)

# gs = plot_gosummaries(edger_files, organism, file.path(output_dir, "edger_gosummaries.png"))

# gprofiler_tables(edger_files, organism, file.path(output_dir, "GProfiler"))
