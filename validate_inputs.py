import os, sys, re, argparse

def convert_newlines(in_file):
	# this only works if the host system has the right Unix utils
	os.system("mac2unix "+in_file)
	os.system("dos2unix "+in_file)

def sanitize_string(string, replacement='_'):
	string = reduce(lambda x,y: x + y, 
		map(lambda x: replacement if ord(x) < 33 else x, string))
	string = string.replace('\\', replacement)
	string = string.replace('/', replacement)
	string = string.replace(' ', replacement)
	# remove repeated replacement chars
	string = replacement.join(re.split(replacement+'+', string))
	return string

def sanitize_file(in_file, num_fields=-1, delim='\t', remove_empty_fields=True, 
	remove_empty_lines=True):
	# num_fields = -1 means we don't care how many fields there are
	fixed_lines = list()
	with open(in_file, 'r') as f:
		for line in f:
			line = line.strip()
			fields = line.split(delim)
			fields = map(lambda x: x.strip(), fields)
			if remove_empty_fields:
				fields = filter(lambda x: len(x) > 0, fields)
			if (len(fields) == 0 and not remove_empty_lines):
				fixed_lines.append('')
			elif (len(fields) > 0 and num_fields < 0) or (len(fields) == num_fields):
				for ii in xrange(len(fields)):
					fields[ii] = sanitize_string(fields[ii])
				fixed_lines.append(delim.join(fields))
	with open(in_file, 'w') as f:
		f.write('\n'.join(fixed_lines))

def extract_fields(in_file, delim=None):
	# return a list of arrays, each representing a row of fields
	lines = list()
	with open(in_file, 'r') as f:
		for line in f:
			line = line.strip()
			if delim is None:
				fields = line.split()
			else:
				fields = line.split(delim)
			if len(fields) > 0:
				lines.append(fields)
	return lines

def validate_read_pairs(read_pairs_file, reads_dir):
	lines = extract_fields(read_pairs_file)
	for fields in lines:
		for entry in fields:
			fastq = os.path.join(reads_dir, entry)
			if not os.path.isfile(fastq):
				raise Exception("Bad %s: refers to missing file %s" 
					% (read_pairs_file, fastq)) 

def validate_sample_files(barcodes_file, samples_described_file, samples_compared_file):
	
	barcodes = extract_fields(barcodes_file)
	samples_described = extract_fields(samples_described_file)
	samples_compared = extract_fields(samples_compared_file, delim='_vs_')

	# check samples_compared format
	for row in samples_compared:
		if len(row) > 2:
			raise Exception("Bad %s: line has more than one _vs_ delimiter" 
				% samples_compared_file)
		elif len(row) < 2:
			raise Exception("Bad %s: line is missing _vs_ delimiter" 
				% samples_compared_file)

	barcode_wells = set([row[0] for row in barcodes])
	barcode_tags = set([row[1]+row[2] for row in barcodes])
	described_samples = set([row[0] for row in samples_described])
	described_wells = set([row[1] for row in samples_described])
	compared_samples = set([row[0] for row in samples_compared]
		+[row[1] for row in samples_compared])
	
	if len(barcode_wells) > len(barcode_tags):
		raise Exception("Bad %s: barcodes (tag+index) are not unique across wells" 
			% barcodes_file)

	if len(described_wells - barcode_wells) > 0:
		raise Exception("Bad %s: refers to wells that are absent in %s" 
			% (samples_described_file, barcodes_file))

	if len(compared_samples - described_samples) > 0:
		raise Exception("Bad %s: refers to samples that are absent in %s"
			% (samples_compared_file, samples_described_file))

def run(read_pairs_file, barcodes_file, 
	samples_described_file, samples_compared_file, reads_dir):
	sanitize_file(read_pairs_file)
	sanitize_file(barcodes_file, num_fields=3)
	sanitize_file(samples_described_file, num_fields=2, remove_empty_lines=False)
	sanitize_file(samples_compared_file, num_fields=1)

	validate_read_pairs(read_pairs_file, reads_dir)
	validate_sample_files(barcodes_file, samples_described_file, samples_compared_file)

	print "All good!"

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("read_pairs_file")
	parser.add_argument("barcodes_file")
	parser.add_argument("samples_described_file")
	parser.add_argument("samples_compared_file")
	parser.add_argument("reads_dir")
	args = parser.parse_args()

	run(args.read_pairs_file, args.barcodes_file, args.samples_described_file, 
		args.samples_compared_file, args.reads_dir)

if __name__ == '__main__':
	main()